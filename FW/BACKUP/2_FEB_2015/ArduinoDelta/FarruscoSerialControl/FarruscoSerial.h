

/*
  FarruscoSerial.cpp - Library for controlling Farrusco the same way than Magabot robotics platform.
  Created by Bruno Serras, Mai 2, 2014.
  Released into the public domain.
  http://artica.cc/
*/


#ifndef FarruscoSerial_h
#define FarruscoSerial_h

#if (ARDUINO >= 100)
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

#include <Wire.h>
#include <Metro.h>
#include <Farrusco.h>


#define REGISTER_CONFIG (16)
#define REGISTER_OUTPUT (16)
//olá

class FarruscoSerial {

	public:
		FarruscoSerial();		
		FarruscoSerial(int ir_connected, int power_connected);

		void actuateMotors(int vel1, int vel2);
		void updateMotors();
		void actuateLEDs(int Red, int Green, int Blue);
		void readBattery();
		void readSonars();
		void changeSonarAddress(char oldAddress, char newAddress);
		void readIR();
		void readBumpers();
		void readClicks();
	
		int irMaxValue;
		int sonarMaxValue;

		bool irState[3];
		int irRead[3];
		bool bumperRead[2];
		int sonarState[6];
		int sonarRead[6];
		int batteryRead;

	private:
		Farrusco farrusco;				// Used to interface the Magabot Protocol

		unsigned long _time;
		unsigned long motorUpdateTime;

		int v1;
		int v2;
		int _sonarReading;
		int _sonarNumber;
		int _sonarId;

		// 1 if YES or 0 if NO
		int IF_IR_CONNECTED;	
		int IF_ANALOG_POWER_CONNECTED;

		int IRValue;

		int maximumServo;
		int minimumServo;
		int slashServo;

		Metro servoMetro; 
		Metro sensorIRMetro;
		Metro servoDelayMetro;
	
};  
#endif
