/*
  FarruscoSerial.cpp - Library for controlling Farrusco the same way than Magabot robotics platform.
  Created by Bruno Serras, Mai 2, 2014.
  Released into the public domain.
  http://artica.cc/
*/

#include <inttypes.h>
#include <Farrusco.h>
  #include <ServoRange.h>
#include "FarruscoSerial.h"





FarruscoSerial::FarruscoSerial()
{
	FarruscoSerial(0, 0);
}

FarruscoSerial::FarruscoSerial(int ir_connected, int power_connected)
{
// Farrusco::Farrusco(int maximumServo, int minimumServo, int delServo, int incServo, int servoPin, bool typeServo, 
// 	int bumperLeftPin, int bumperRightPin, int irPin, int motorA0, int motorA1, int motorB0, int motorB1)
	//farrusco = Farrusco(150, 30, 25, 2, 8, 7, 0, 9, 6, 11, 3, 5);
	//ServoRange(int maximumServo, int minimumServo, int delayServo, int incrServo, int servoPin, bool typeServo);
	//farrusco.neck.Initializer(150, 30, 25, 2, 9, true);	
	
	// Only way found to update the servo range value "Farrusco Constructor" not working
	//neck = ServoRange(maximumServo, minimumServo, delServo, incServo, servoPin, typeServo);
	maximumServo = 160;
	minimumServo = 20;
	slashServo = (maximumServo - minimumServo) / 5;
	farrusco.neck = ServoRange(maximumServo, minimumServo, 5, 5, 9, true);

	// --------------------------------------------------------------------------- METRO
	servoMetro = Metro(25); 
	sensorIRMetro = Metro(50);
	servoDelayMetro = Metro(25);


	IF_IR_CONNECTED = ir_connected;
	IF_ANALOG_POWER_CONNECTED = power_connected;

	if (!IF_IR_CONNECTED)
	{
		for (int i = 0; i < 3; i++)
		{
			irRead[i] = irMaxValue / 2;
			irState[i] = (irRead[i]>irMaxValue)?true:false;
		}
	}

	if (!IF_ANALOG_POWER_CONNECTED)
	{
		 batteryRead = 800;	// --------------------------- !!!!!!
	}


	// initialize the digital pin as an output.
	// Pin 13 has an LED connected on most Arduino boards:
	// FARRUSCO CONVERTION: Can be the same ... 
	pinMode(13, OUTPUT);    
	pinMode(12, OUTPUT);

	//RGB led pins
	// FARRUSCO CONVERTION: NOT IMPLEMENTED ... 
	//	pinMode(11, OUTPUT);     
	//	pinMode(10, OUTPUT); 
	//	pinMode(9, OUTPUT);   
	// analogWrite(11,255);
	// analogWrite(10,255);
	// analogWrite(9,255);
  
	//Bumpers
	// FARRUSCO CONVERTION: PIN 8 AND 9
	pinMode(8, INPUT);
	pinMode(9, INPUT);
	// pinMode(2, INPUT);
	// pinMode(3, INPUT);
	// pinMode(4, INPUT);
	// pinMode(5, INPUT);

	//IR
	// FARRUSCO CONVERTION: PIN 10, 4 and 2
	pinMode(10, OUTPUT);     // Analog Pin 1
	pinMode(4, OUTPUT);      // Analog Pin 2
	pinMode(2, OUTPUT);      // Analog Pin 3	
	// pinMode(6, OUTPUT);     
	// pinMode(7, OUTPUT);     
	// pinMode(8, OUTPUT);     

	// FARRUSCO CONVERTION:
	// No Wire - Not Connected By I2C - Farrusco
	// Wire.begin();

	_sonarReading = 0;
	_sonarNumber = 0;
	_sonarId = 1;

	irMaxValue = 1023;
}

//**************************//
//******Actuate motors******//
//**************************//
void FarruscoSerial::actuateMotors(int vel1, int vel2)
{
  v1=vel1;
  v2=vel2; 
  vel2 = -vel2;
 
  // FARRUSCO: Wait for update .... 

  // byte v1b1 = vel1 >> 8;
  // byte v1b2 = vel1 & 0xFF;
  // byte v2b1 = vel2 >> 8;
  // byte v2b2 = vel2 & 0xFF;
  
  // Wire.beginTransmission(0x15);
  // Wire.write((byte)0);
  // Wire.write(v1b1);
  // Wire.write(v1b2);
  // Wire.write(1);    //  high byte
  // Wire.endTransmission();
  
  // Wire.beginTransmission(0x16);
  // Wire.write((byte)0);
  // Wire.write(v2b1);
  // Wire.write(v2b2);
  // Wire.write(1);    //  high byte
  
  // Wire.endTransmission();

  motorUpdateTime = millis()+100;
}

//**************************//
//******Update motors******//
//**************************//
void FarruscoSerial::updateMotors()
{
  if(motorUpdateTime <millis())
  {
	  if(v1 != 0 && v2 != 0)
		  farrusco.DiffTurn(v1,v2);

	  motorUpdateTime = millis()+100;
  }
  
}
//************************//
//******Actuate LEDs******//
//************************//
void FarruscoSerial::actuateLEDs(int Red, int Green, int Blue)
{
	// NOT IMPLEMENTED !!!!!!!

	Red = 255 - Red;
	Green = 255 - Green;
	Blue = 255 - Blue;
	
	// analogWrite(11,(unsigned char) Green);
	// analogWrite(10,(unsigned char) Red);
	// analogWrite(9,(unsigned char) Blue);



}

//********************************//
//******Read Battery Status*******//
//********************************//
void FarruscoSerial::readBattery()
{
	//FARRUSCO CHANGE: Analog(4) !!!!!

	if (!IF_ANALOG_POWER_CONNECTED) return;

	//batteryRead = analogRead(3);
	//batteryRead = analogRead(3);
    batteryRead = analogRead(4);
    batteryRead = analogRead(4);

   
          
	if (batteryRead<500) 
	{
		digitalWrite(13, LOW);
		digitalWrite(12, LOW);
		actuateMotors(0,0);
	}
	else if (batteryRead<766)
	{
		digitalWrite(13, HIGH);
		digitalWrite(12, LOW);
	}
	else if (batteryRead<830)
	{
		digitalWrite(13, HIGH);
		digitalWrite(12, HIGH);
	}
	else 
	{
		digitalWrite(13, LOW);
		digitalWrite(12, HIGH);
	}
}

//***************************************//
//*****SONARS READ AND CHANGE ADRESS*****//
//***************************************//
void FarruscoSerial::readSonars()
{ 
	int ServPos = farrusco.GetPosition();

	

	if (sensorIRMetro.check() == 1) { 
    	IRValue = (4800/(farrusco.IRValueCheck()-20));
  	}
  
  	if (servoMetro.check() == 1) { 
    	farrusco.Range();
  	}

	if (ServPos < (minimumServo + slashServo))
	{
		sonarRead[1]  = IRValue;
		sonarState[1] = true;	// Allways true on Farrusco   	

	} else if (ServPos < (minimumServo +(slashServo * 2)) )
	{
		sonarRead[2]  = IRValue;
		sonarState[2] = true;	// Allways true on Farrusco   	
	
		
	
	} else if (ServPos < (minimumServo +(slashServo * 3)) )
	{
		sonarRead[3]  = IRValue;
		sonarState[3] = true;	// Allways true on Farrusco   

		
	
	} else if (ServPos < (minimumServo +(slashServo * 4)) )
	{
		sonarRead[4]  = IRValue;
		sonarState[4] = true;	// Allways true on Farrusco 
	
		
	
	} else if (ServPos < (minimumServo +(slashServo * 5))) 
	{
		sonarRead[5]  = IRValue;
		sonarState[5] = true;	// Allways true on Farrusco  
		
	} 


	



	// if (_sonarNumber==0)
	// {
	// 	Wire.beginTransmission(0x70+_sonarId);
	// 	Wire.write((byte)0);
	// 	Wire.write(0x51);
	// 	Wire.endTransmission();
	// 	_sonarNumber=1;
	// 	_time = millis()+50; //
	// }
	// else if (millis()>_time)
	// {
	// 	// step 3: instruct sensor to return a particular echo _sonarReading
	// 	Wire.beginTransmission(0x70+_sonarId); // transmit to device #112
	// 	Wire.write(0x02);             // sets register pointer to echo #1 register (0x02)
	// 	Wire.endTransmission();      // stop transmitting

	// 	// step 4: request _sonarReading from sensor
	// 	Wire.requestFrom(0x70+_sonarId, 2);    // request 2 chars from slave device #112

	// 	// step 5: receive _sonarReading from sensor
	// 	if(2 <= Wire.available())    // if two chars were received
	// 	{
	// 		_sonarReading = Wire.read();  // receive high char (overwrites previous _sonarReading)
	// 		_sonarReading = _sonarReading << 8;    // shift high char to be high 8 bits
	// 		_sonarReading |= Wire.read(); // receive low char as lower 8 bits

	// 		sonarRead[_sonarId]=_sonarReading;
	// 		sonarState[_sonarId] = (sonarRead[_sonarId]>sonarMaxValue)?true:false;   
	// 		_sonarNumber=0;
	// 		_sonarId++;
	// 		if (_sonarId==6) _sonarId=1; //_sonarId==6
	// 	}
	// 	else
	// 	{
	// 		_sonarNumber=0;
	// 		_sonarId++;
	// 		if (_sonarId==6) _sonarId=1; //_sonarId==6      
	// 	}
	// }
}

//Change I2C sonarRead Address
void FarruscoSerial::changeSonarAddress(char oldAddress, char newAddress)
{

	// NOT IMPLEMENTED IN FARRUSCO

	// Wire.beginTransmission(oldAddress);
	// Wire.write((byte)0x00);
	// Wire.write(0xA0);
	// Wire.endTransmission();

	// Wire.beginTransmission(oldAddress);
	// Wire.write((byte)0x00);
	// Wire.write(0xAA);
	// Wire.endTransmission();

	// Wire.beginTransmission(oldAddress);
	// Wire.write((byte)0x00);
	// Wire.write(0xA5);
	// Wire.endTransmission();

	// Wire.beginTransmission(oldAddress);
	// Wire.write((byte)0x00);
	// Wire.write(newAddress);
	// Wire.endTransmission();
}

//*************************************//
//****IR ground sensors read***********//
//*************************************//
void FarruscoSerial::readIR() 
{
	// FARRUSCO: Changed WRITE to 10,4 and 2 and Analog read to 1,2 and 3

	if (!IF_IR_CONNECTED) return;

	digitalWrite(10, HIGH);
	analogRead(1);
	irRead[0] = analogRead(1);
	irState[0] = (irRead[0]>irMaxValue)?true:false;
	digitalWrite(10, LOW);
  
	digitalWrite(4, HIGH);
	analogRead(2);
	irRead[1] = analogRead(2);
	irState[1] = (irRead[1]>irMaxValue)?true:false;
	digitalWrite(4, LOW);
  
	digitalWrite(2, HIGH);
	analogRead(3);
	irRead[2] = analogRead(3);
	irState[2] = (irRead[2]>irMaxValue)?true:false;
	digitalWrite(2, LOW);
}

//***************************************//
//****Front bumpers read function********//
//***************************************//
void FarruscoSerial::readBumpers()
{
	//FARRUSCO : 8 and 7
	bumperRead[0] = (digitalRead(8)==1)?true:false;
	bumperRead[1] = (digitalRead(7)==1)?true:false;
	bumperRead[2] = (digitalRead(8)==1)?true:false;
	bumperRead[3] = (digitalRead(7)==1)?true:false;

}


//***************************//
//****** Odometer ***********//
//***************************//
void FarruscoSerial::readClicks()
{


	// //primeira parte
	// Wire.beginTransmission(0x15);
	// Wire.write(0x19);
	// Wire.write(1);
	// Wire.endTransmission();

	// delay(1);  

	// Wire.beginTransmission(0x16);
	// Wire.write(0x19);
	// Wire.write(1);  
	// Wire.endTransmission();

	// delay(1); 

	// Wire.beginTransmission(0x15); // transmit to device 0x15
	// Wire.write(0x15);             // sets register pointer to echo #1 register (0x15)
	// Wire.endTransmission();

	// Wire.requestFrom(0x15, 2);
  
	// if(2 <= Wire.available())    // if two chars were received
	// {
	// 	Serial.write(Wire.read());
	// 	Serial.write(Wire.read());
	// }
  
	// Wire.beginTransmission(0x16); // transmit to device 0x16
	// Wire.write(0x15);             // sets register pointer to echo #1 register (0x15)
	// Wire.endTransmission();
  
	// Wire.requestFrom(0x16, 2);
	// if(2 <= Wire.available())    // if two chars were received
	// {
	// 	Serial.write(Wire.read());
	// 	Serial.write(Wire.read());
	// }
	// //primeira parte
	// Wire.beginTransmission(0x15);
	// Wire.write(0x14);
	// Wire.write((byte)0);
	// Wire.endTransmission();
	// delay(1);  

	// Wire.beginTransmission(0x16);
	// Wire.write(0x14);
	// Wire.write((byte)0);
	// Wire.endTransmission();
	// delay(1);  
}
