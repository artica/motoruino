//Bumpers - Allows to read the value of the distance sensors

#ifndef GyroBumper_h
#define GyroBumper_h

#include "Arduino.h"

// Default pin for the left bumper
#define _DEFAULT_BUMPER_LEFT_PIN 8
// Default pin for the right bumper
#define _DEFAULT_BUMPER_RIGHT_PIN 7

class GyroBumper
{
	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration
		// Default constructor, uses pins _DEFAULT_BUMPER_LEFT_PIN and
		// _DEFAULT_BUMPER_RIGHT_PIN as default
		GyroBumper();
		// Custom constructor, specify the pins for both bumpers
		GyroBumper(int pinBumperLeft, int pinBumperRight);
		// Configure the pins for the front bumpers
		void Begin( int pinBumperLeft, int pinBumperRight );
				
		// ---------------------------------------------------------------------
		// Bumper sensor methods
		// Check if the left bumper is active (active = TRUE)
		bool CheckLeftBumper();
		// Check if the right bumper is active (active = TRUE)
		bool CheckRightBumper();
		
	private:
		// Configures all the necessary variables for the class
		void configure( int pinBumperLeft, int pinBumperRight );
		// Initializes the input pins for the bumpers
		void initialize();
		// IO pin used for the left bumper
		int _pinBumperLeft;
		// IO pin used for the right bumper
		int _pinBumperRight;
		// Is the servo initialized?
		// The initialization occurs when Begin is called, or when it is used the first time
		bool _initialized;
};

#endif
