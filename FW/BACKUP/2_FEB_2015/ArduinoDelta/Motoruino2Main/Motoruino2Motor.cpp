#include "Motoruino2Motor.h"

// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor, initializes the motors in the default pins (3,5,6,11) 
Motoruino2Motor::Motoruino2Motor()
{
	// Flag revealing that all configurations have ended - Last thing to do here
	Comm.begin();
	_initialized = true;
}



// ---------------------------------------------------------------------
// Movement methods
// Stops the movement completely
void Motoruino2Motor::StopMovement()
{
	// @TODO fazer isto no micro secundario
	MoveMotors(0, 0);
}

//Farrusco go forward
void Motoruino2Motor::MoveForward(unsigned int distance, byte speed)
{
	// @TODO fazer isto no micro secundario
	MoveMotors(speed, speed);
}

//Farrusco go backward
void Motoruino2Motor::MoveBackward (unsigned int distance, byte speed)
{
	// @TODO fazer isto no micro secundario
	MoveMotors(-speed, -speed); 
}

// Moves forward while turning on a specific angle, in degrees
// With angle = 0 degrees the movement is straight forward
void Motoruino2Motor::MoveForwardArc (int angularSpeed, unsigned int distance, byte speed)
{
	// @TODO fazer isto no micro secundario
	// Fazer cenas fixes aqui! :P
}

// Farrusco turns in order to face a specific heading
// The rotation direction is chosen in order to minimize the rotation
bool Motoruino2Motor::TurnTo (int angle, byte speed)
{
	byte Buffer[8];
	byte* ReceivedData;

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'D'; Buffer[2] = 'E'; Buffer[3] = 'G';
	for (int x = 0; x < 2; x++ )				// Fills the Buffer MSB goes first
	{
		Buffer[4 + x] = (0x00FF & angle);
		angle = angle >> 8;
	}
	Buffer[6] = speed;

	// Send SPWM Command with 4 Bytes data and no reply buffer request
	ReceivedData = Comm.sendData(Buffer, 7, 100, 6);

	// Validate the received message
	for (int i = 0; i < 4; i++) if (Buffer[i] != ReceivedData[i]) return false;
	if ((ReceivedData[4] != 'O') || (ReceivedData[5] != 'K')) return false;

	return true;
}

//Farrusco turn right
void Motoruino2Motor::TurnRight (int angle, byte speed)
{
	// @TODO fazer isto no micro secundario
	MoveMotors(-speed, speed);
}	

//Farrusco turn left		
void Motoruino2Motor::TurnLeft(int angle, byte speed)
{


	// @TODO fazer isto no micro secundario
	MoveMotors(speed, -speed);
}

//Receive each motor velocity as argument
void Motoruino2Motor::MoveMotors(int speedLeft, int speedRight)
{
	byte _IN1_M1, _IN2_M1, _IN1_M2, _IN2_M2;

	// Sets the correct rotation of the motor
	speedLeft *= M1_SIGNAL;
	speedRight *= M2_SIGNAL;

	// Sets the M2 (Left) Motor
	if (speedLeft < 0)
	{  
		_IN1_M2 = 0;
		_IN2_M2 = (byte) speedLeft * -1;
	} 
	else
	{  
		_IN1_M2 = (byte) speedLeft;
		_IN2_M2 = 0;
	}
 
	// Sets the M1 (Right) Motor
	if (speedRight < 0) 
	{
		_IN1_M1 = 0;
		_IN2_M1 = (byte) speedRight * -1;
	}
	else
	{
		_IN1_M1 = (byte) speedRight;
		_IN2_M1 = 0;
	}



	// Update the Sensor_uC with the new MotorOrder
	HBridgePWMSet(_IN1_M1, _IN2_M1, _IN1_M2, _IN2_M2);
}

//Set the PWM of the H Bridge independently
void Motoruino2Motor::HBridgePWMSet(byte IN1_M1, byte IN2_M1, byte IN1_M2, byte IN2_M2)
{
	byte Buffer[8];
	Comm.begin();

	// Construct the Command Buffer
	Buffer[0] = 'S'; Buffer[1] = 'P'; Buffer[2] = 'W'; Buffer[3] = 'M';
	Buffer[4] = IN1_M1; Buffer[5] = IN2_M1; Buffer[6] = IN1_M2; Buffer[7] = IN2_M2;


	// Send SPWM Command with 4 Bytes data and no reply buffer request
	Comm.sendData(Buffer, 8, 0, 0);


}

