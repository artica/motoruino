#include "Motoruino2Wire.h"


// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor, initializes the motors in the default pins (3,5,6,11)
Motoruino2Comm::Motoruino2Comm()
{
	_initialized = false;
}

void Motoruino2Comm::commConfig()
{
	if (_initialized) return;



	// Flag revealing that all configurations have ended - Last thing to do here
	_initialized = true;
}

// ---------------------------------------------------------------------
// SendFunction
byte* Motoruino2Comm::sendData(byte* array, byte Size, unsigned short replyDelay, byte ReplySize)
{
	int i = 0;
	long start = 0;

	Wire.commConfig();
	Wire.beginTransmission(SLAVE_ADD);

	for (int x = 0; x < Size; x++ )
	{
		Wire.write(array[x]);
	}

	Wire.endTransmission();    // stop transmitting

	delay(3);
	delay(replyDelay);
	// request ReplySize bytes from slave device #SLAVE_ADD
	if (ReplySize){

		Wire.requestFrom(SLAVE_ADD, (int) ReplySize);

		// Wait for Reply
		start = millis();
		while (Wire.available() && (millis() - start < 10000))    // slave may send less than requested
		{
			replyBuffer[i++] = Wire.read(); // receive a byte as character
		}

	}

	return replyBuffer;

}


