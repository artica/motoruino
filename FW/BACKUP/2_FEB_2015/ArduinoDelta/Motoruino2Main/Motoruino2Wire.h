//Communications

#ifndef Motoruino2Wire_h
#define Motoruino2Wire_h

#include "Arduino.h"
#include <Wire.h>
#include <Metro.h>


#define SLAVE_ADD 2
#define COMM_BUFF_SIZE 10

class Motoruino2Comm
{
	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration
		// Default constructor
		Motoruino2Comm();

		void commConfig();

		// ---------------------------------------------------------------------
		// Sends Data to I2C
		byte* sendData(byte* array, byte Size, unsigned short replyDelay, byte ReplySize);
		// ---------------------------------------------------------------------
		// Test Response from I2C
		bool hasResponse();
		// ---------------------------------------------------------------------
		// Gets Response from I2C
		byte* getResponse();


		byte replyBuffer[COMM_BUFF_SIZE];

	private:
		bool _initialized;


};
#endif
