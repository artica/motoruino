#include "Motoruino2IMU.h"

// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor
Motoruino2IMU::Motoruino2IMU() 
{
	Serial.begin(115200);
	Comm.begin();
	_initialized = true;
}


// ---------------------------------------------------------------------
bool Motoruino2IMU::StartGyro()
{
	byte Buffer[4];
	byte * ReceivedData;

	// Construct the Command Buffer
	Buffer[0] = 'S'; Buffer[1] = 'T'; Buffer[2] = 'T'; Buffer[3] = 'G';

	// Send the Command
	ReceivedData = Comm.sendData(Buffer, 4, 0, 4 + 2);

	// Validate the received message
	for (int i; i < 4; i++) if (Buffer[i] != ReceivedData[i]) return false;
	if ((ReceivedData[4] != 'O') || (ReceivedData[5] != 'K')) return false;

	return true;
}

bool Motoruino2IMU::StopGyro()
{
	byte Buffer[4];
	byte * ReceivedData;

	// Construct the Command Buffer
	Buffer[0] = 'S'; Buffer[1] = 'T'; Buffer[2] = 'P'; Buffer[3] = 'G';

	// Send the Command
	ReceivedData = Comm.sendData(Buffer, 4, 0,  4 + 2);

	// Validate the received message
	for (int i; i < 4; i++) if (Buffer[i] != ReceivedData[i]) return false;
	if ((ReceivedData[4] != 'O') || (ReceivedData[5] != 'K')) return false;

	return true;
}

bool Motoruino2IMU::CalibrateGyro()
{
	byte Buffer[4];
	byte * ReceivedData;

	// Construct the Command Buffer
	Buffer[0] = 'C'; Buffer[1] = 'A'; Buffer[2] = 'L'; Buffer[3] = 'G';

	// Send the Command
	ReceivedData = Comm.sendData(Buffer, 4, 6500, 4 + 2);

	Serial.println("received");

	Serial.write(ReceivedData[0]);Serial.write(ReceivedData[1]);
	Serial.write(ReceivedData[2]);Serial.write(ReceivedData[3]);
	Serial.write(ReceivedData[4]);Serial.write(ReceivedData[5]);
	Serial.println(" ");

	// Validate the received message
	for (int i=0; i < 4; i++) if (Buffer[i] != ReceivedData[i]) return false;
	if ((ReceivedData[4] != 'O') || (ReceivedData[5] != 'K')) return false;

	return true;
}

// Get the current heading for farrusco (in degrees)
long Motoruino2IMU::getGyroHeading()
{
	byte Buffer[8];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'G'; Buffer[2] = 'H'; Buffer[3] = 'D';

	return GetLongValue(Buffer);
}

long Motoruino2IMU::getGyroPitch()
{
	byte Buffer[8];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'P'; Buffer[2] = 'I'; Buffer[3] = 'T';

	return GetLongValue(Buffer);

}

long Motoruino2IMU::getGyroRoll()
{
	byte Buffer[8];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'R'; Buffer[2] = 'O'; Buffer[3] = 'L';

	return GetLongValue(Buffer);

}

// Resets the current heading referential, making the current heading = 0 degrees
bool Motoruino2IMU::ResetGyroscope()
{
	byte Buffer[4];
	byte * ReceivedData;

	// Construct the Command Buffer
	Buffer[0] = 'R'; Buffer[1] = 'S'; Buffer[2] = 'T'; Buffer[3] = 'G';

	// Send the Command
	ReceivedData = Comm.sendData(Buffer, 4, 0,  4 + 2);

	// Validate the received message
	for (int i; i < 4; i++) if (Buffer[i] != ReceivedData[i]) return false;
	if ((ReceivedData[4] != 'O') || (ReceivedData[5] != 'K')) return false;

	return true;
}

// Get the raw values from the accelerometer and gyroscope for the different axis
short Motoruino2IMU::getAccelerometerX()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'A'; Buffer[2] = 'C'; Buffer[3] = 'X';

	return GetLongValue(Buffer);
}

short Motoruino2IMU::getAccelerometerY()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'A'; Buffer[2] = 'C'; Buffer[3] = 'Y';

	return GetShortValue(Buffer);
}

short Motoruino2IMU::getAccelerometerZ()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'A'; Buffer[2] = 'C'; Buffer[3] = 'Z';

	return GetLongValue(Buffer);
}

short Motoruino2IMU::getGyroscopeX()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'G'; Buffer[2] = 'Y'; Buffer[3] = 'X';

	return GetLongValue(Buffer);
}

short Motoruino2IMU::getGyroscopeY()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'G'; Buffer[2] = 'Y'; Buffer[3] = 'Y';

	return GetLongValue(Buffer);
}

short Motoruino2IMU::getGyroscopeZ()
{
	byte Buffer[6];

	// Construct the Command Buffer
	Buffer[0] = 'G'; Buffer[1] = 'G'; Buffer[2] = 'Y'; Buffer[3] = 'Z';

	return GetLongValue(Buffer);
}

long Motoruino2IMU::GetLongValue(byte Buff[8])
{
	byte * ReceivedData;
	long Data = 0;

	// Send the Command
	ReceivedData = Comm.sendData(Buff, 4, 0, 4 + 4);

	// Validate and process the received message - Heading
	for (int i = 0; i < 4; i++)
	{
		if (Buff[i] != ReceivedData[i]) return 0xFFFFFFFF;
	}

	for (int i = 4; i < 8; i++)
	{
		Data = Data | (ReceivedData[i] << ((i-4)*8));
	}

	return Data;
}

short Motoruino2IMU::GetShortValue(byte Buff[6])
{
	byte * ReceivedData;
	short Data = 0;

	// Send the Command
	ReceivedData = Comm.sendData(Buff, 4, 0, 4 + 2);

	// Validate and process the received message - Heading
	for (int i = 0; i < 4; i++)
	{
		if (Buff[i] != ReceivedData[i]) return false;
	}

	for (int i = 4; i < 6; i++)
	{
		Data = Data | (ReceivedData[i] << ((i-4)*8));
	}

	return Data;
}

