/*
 * GyroMotor.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#ifndef Motoruino2SMotor_h
#define Motoruino2SMotor_h

#include "Arduino.h"
#include <Metro.h>

// DEFINITIONS OF THE MOTOR ROTATION
#define M1_DIR 1
#define M2_DIR -1

// DEFINITIONS OF THE PINS
#define ENC1_INT 	2		// Encoder M1 Interrupt activated In
#define ENC1_IO 	A3		// Encoder M1 IO In -> OLD Pin4 -> See LSM330DLC.h -> Now RX
#define ENC2_INT 	3		// Encoder M2 Interrupt activated In
#define ENC2_IO 	A2		// M2 PWM output

#define M1_IN1 		5		// M1 PWM output
#define M1_IN2 		6		// M1 PWM output
#define M2_IN1 		10		// M2 PWM output
#define M2_IN2 		9		// M2 PWM output

#define MOTOR_ADC 	A6		// ADC for the L298P Resistor for ShortCircuit/Consumption Detection

// Default PID Controller Factors
#define KP	0.8
#define KD	0.1
#define KI	0.0

// Interrupt Routines Defines
#define TIMER_RESET 10				// How many "ticks" before reset the Encoder
#define AVERAGE_RACIO 1				// 0.3 = 30% of the read value

#define TIMER_RACIO 8

#define DEBUG A1

class Motoruino2SPID
{
	public:
		Motoruino2SPID();

		void Config(float _Kp = KP, float _Kd = KD, float _Ki = KI );

		void SetupPID1(float factorProportional, float factorDerivative, float factorIntegrative);
		float UpdatePID1(float cte);

		void SetupPID2(float factorProportional, float factorDerivative, float factorIntegrative);
		float UpdatePID2(float cte);

	private:

		volatile float pidPrevCTE1, pidSumCTE1;
		volatile float pidPrevCTE2, pidSumCTE2;
		volatile bool pidFirstTime1;
		volatile bool pidFirstTime2;
		volatile float factorP1,factorD1, factorI1;
		volatile float factorP2,factorD2, factorI2;
		volatile float pidRetValue1 ;
		volatile float pidRetValue2 ;

};

class Motoruino2SMotor
{
	public:

		// Types of encoders:
		// OPTICAL WHEEL - optical encoders directly applied to wheel: interrupt on change, measure by time
		// OPTICAL MOTOR - optical encoder applied on the "back" of the motor: interrupt on rising, measure by tick count
		// HALL_EFFECT_MOTOR - same as above
		enum EncoderMode {SLOW_ENCODER , FAST_ENCODER, HALL_EFFECT_MOTOR};

		// TODO: Block the Interrupts and implement the methods to get values
		static volatile float desiredFrequency1;
		static volatile float desiredFrequency2;

		static volatile signed short counterEncoder1;
		static volatile signed short counterEncoder2;

		static volatile float frequency1;
		static volatile float frequency2;

		static volatile float distance1;
		static volatile float distance2;

		// Current PWM Values for the motors
		unsigned char PWM_Value_M1_IN1;
		unsigned char PWM_Value_M1_IN2;
		unsigned char PWM_Value_M2_IN1;
		unsigned char PWM_Value_M2_IN2;

		// Constructor
		Motoruino2SMotor();

		// Config Methods
		void Config();
		void ConfigEncoders(enum EncoderMode _mode, short _reduction_ratio  , float _wheels_diam_cm );

		// Methods
		long GetSpeed(unsigned char motor);
		void SetSpeed(unsigned char motor, long _DesiredFrequency);

		void ResetDistance(unsigned char motor);
		long GetDistance(unsigned char motor);

		void MOTOR_SET(int speedLeft, int speedRight);

	private:

		static Motoruino2SPID pid;

		// Loval var for the type of encoder selected
		volatile enum EncoderMode mode;

		// Speed control var to apply to motor when "set speed" - PID reply
		static volatile int motorSpeed1;
		static volatile int motorSpeed2;

		static volatile short reduction_ratio;			// = resolution of encoder per loop
		volatile float wheels_diam;						// = wheel diameter selected on the configuration
		static volatile float perimeter_pertick;		// = (2*pi*(wheels_diam/2)/reduction_ratio)

		// aux var to decrement the ticks before reset encoder count
		static volatile unsigned short timer1Dec;

		volatile bool isEncoderDefined;					// encoders already configured... "fail safe flag"

		static unsigned long oldMicros_1;
		static bool period1Updated;
		static unsigned long oldMicros_2;
		static bool period2Updated;

		static volatile bool automatic_motors_control;			// autonomous speed control flag ... set speed = true; set pwm = false

		static void PWM_MOTOR_SET(int speedLeft, int speedRight);

		static void InterruptTimer1_Motor();
		static void InterruptEncoder1_Motor();
		static void InterruptEncoder2_Motor();

		static void InterruptTimer1_Wheel();
		static void InterruptEncoder1_Wheel();
		static void InterruptEncoder2_Wheel();







};




#endif

