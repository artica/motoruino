/*
 * GyroPower.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#ifndef Motoruino2SPower_h
#define Motoruino2SPower_h

#include "Arduino.h"

#define WAKE_IN 		4		//
#define SLEEP_CTL 		7		//
#define BATTERY_ADC 	A7

class Motoruino2SPower
{

	public:
		// Constructor
		Motoruino2SPower();

		// Config
		void Config();


	private:

};

#endif


