/*
 * 	ARTICA CC
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

bool ReachAngle(long Angle, byte Speed)
{
	long Heading = (long)onboardIMU.GetGyroHeading();
	long Speed_Aux = 0;

	Serial.println("A");

	//Serial.print(Angle); Serial.print(" "); Serial.print(Heading);Serial.print(" ");

	// Reset Heading -> Positions Heading on Zero and Replace the Angle
	Angle = Angle - Heading;

	if (Angle > 18000)
		Angle =  Angle - 36000;
	else if (Angle < -18000)
		Angle =  Angle + 36000;

	//Serial.println(Angle);

	// GOTO Positive Angle
	if (Angle > 0)
	{

		 if (Angle > ANGLE_MEDIUM)
		{
			motor.MOTOR_SET(Speed, 0);

		}
		else
		{
			Speed_Aux = ((long)Speed * Angle)/(long)(-1 * ANGLE_MEDIUM)  + (long)255;
			if (Speed_Aux > 255) Speed_Aux = 255;
			motor.MOTOR_SET(Speed, (byte)Speed_Aux);
		}


	}
	// GOTO Positive Angle
	else
	{

		if (Angle < -ANGLE_MEDIUM)
		{
			motor.MOTOR_SET( 0, Speed);

		} else {

			Speed_Aux = ((long)Speed * Angle)/(long)(ANGLE_MEDIUM)  + (long)255;

			if (Speed_Aux > 255) Speed_Aux = 255;
			motor.MOTOR_SET( (byte)Speed_Aux, Speed);
		}

	}

	if ((Angle > -ANGLE_OK) && (Angle < ANGLE_OK))
		return true;
	else
		return false;
}
