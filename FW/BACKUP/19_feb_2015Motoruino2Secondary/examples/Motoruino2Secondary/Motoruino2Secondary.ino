/*
 * 	ARTICA CC
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#include "Arduino.h"
#include <avr/sleep.h>
#include <avr/interrupt.h>

#include "Metro.h"
#include "Wire.h"
#include "TimerOne.h"

#include "LSM9DS0.h"
#include "Motoruino2SMotor.h"
#include "Motoruino2SPower.h"
#include "Motoruino2SSPI.h"

// ------------------------- DEFINES -------------------------
#define GP_OUT A1

#define BLINKTIME 			1000	// Printout Debug Flag (ms)
#define NAVIGATIONTIME 		10		// Pooling timer for navigation (ms)

#define ANGLE_OK 100				// Reach the Angle: default: 1 deg x 10 => example: 10deg = 1000 or 1deg = 100
#define ANGLE_MEDIUM 8000			// Angle from which it will go to full speed on navigation; default: 80 deg (8000)

#define BUFFER_SIZE 	10			// I2C Buffer size
#define WIRE_ADDRESS 	2			// Slave I2C address

// --------------------- OBJECTS/GLOBAL ----------------------
Motoruino2LSM9DS0				onboardIMU;
Motoruino2SMotor 	motor;		// Motor, PID and encoder Functions
Motoruino2SPower 	power;		// Power Functions
Motoruino2SSPI		spi;		// SPI Functions

// Metro Update
Metro MetroUpdateTime = Metro(BLINKTIME);
Metro MetroNavigationTime = Metro(NAVIGATIONTIME);

// Wire Comm
char receivedBuffer[BUFFER_SIZE];
char sendBuffer[BUFFER_SIZE];

unsigned char sendLength = 0;

// Comm Protocol
enum _ReplyOrders { NO_REPLY , REPLY_CALIBRATE_IMU, REPLY_GET_HEADING, REPLY_GOTO_DEGREES};
enum _ReplyOrders ProcessLaterOrder = NO_REPLY;

// Pooling Update Flags
bool startFlagIMU = false; 				// Get IMU Values?
bool startNavigateToAngle = false;	// Goto Angle?
bool readyFlagToReply = false;			// Finished processing the reply?

// Navigation
int goalAngle = 0;
byte goalSpeed = 0;

// Aux Global Vars
long Data = 0;

// --------------------- SETUP ----------------------
void setup() {


//   MCUCR |= (1<<BODS) | (1<<BODSE);
//   MCUCR &= ~(1<<BODSE);  // must be done right before sleep

	Serial.begin(115200);

	pinMode(GP_OUT, OUTPUT);

	pinMode(SLEEP_CTL, OUTPUT);
	digitalWrite(SLEEP_CTL, HIGH);

	// Communication Configuration
	commConfig();

	// Object Configurations
	motor.Config();
	motor.ConfigEncoders(motor.SLOW_ENCODER, 10, 4.25);
	//motor.ConfigEncoders(motor.OPTICAL_MOTOR, 2000, 4.25);

	power.Config();

	spi.config();
	onboardIMU.config();
	onboardIMU.calibrateGyro();

	// Pools/Flags init
	startFlagIMU = true;
	startNavigateToAngle = false;
	readyFlagToReply = false;

	// Main Values init
	goalAngle = 0;
	goalSpeed = 0;

	// Aux Values init
	Data = 0;

	//motor.MOTOR_SET(0,100);
	// set Motor 1 20
	//motor.SetSpeed(1,-40);
	motor.SetSpeed(2,200);


}


bool outstate = false;

// --------------------- LOOP ----------------------
void loop() {

	//outstate = !outstate;
	//digitalWrite(GP_OUT, outstate);

	digitalWrite(SLEEP_CTL, HIGH);

//
//	sleep_enable();
//	/* 0, 1, or many lines of code here */
//	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
//	cli();
//	//sleep_bod_disable();
//	//sei();
//	sleep_cpu();
//	/* wake up here */
//	sleep_disable();


	// POOLING Functions
	// - IMU
	if (startFlagIMU)
	{
		onboardIMU.updateAccelerometer();
		onboardIMU.updateGyroscope();
		onboardIMU.updateMagnetometer();
	}

	// - Navigation
	if (startNavigateToAngle)					// Navigation Pooling function
		if (MetroNavigationTime.check() == 1)
			if (ReachAngle((long) goalAngle, goalSpeed)) startNavigateToAngle = false;

	// - Wire Communications (Master)
	//UpdateComm();

	// TEST Functions to print
	if (MetroUpdateTime.check() == 1)
	{

		Serial.print("\tH:"); Serial.print(onboardIMU.getGyroHeading()/100);
		Serial.print("\tAz:"); Serial.print(onboardIMU.getAccelAvgZ());
		Serial.print("\tM:"); Serial.print(onboardIMU.getMagHeading());
		Serial.print("\tT:"); Serial.print(onboardIMU.getTemp());

		Serial.print("D:");

		Serial.print(motor.distance1);Serial.print("\t");
		Serial.print(motor.distance2);Serial.print("\t");
		Serial.print(motor.frequency1);Serial.print("\t");
		Serial.print(motor.frequency2);Serial.print("\t");
		Serial.println("");

		motor.ResetDistance(1);
		motor.ResetDistance(2);

		Serial.println();

	}

}





