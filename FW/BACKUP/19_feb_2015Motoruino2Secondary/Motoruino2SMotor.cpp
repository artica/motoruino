/*
 * GyroMotor.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#include "Arduino.h"
#include "Motoruino2SMotor.h"
#include "TimerOne.h"
#include <digitalWriteFast.h>  // library for high performance reads and writes by jrraines
                               // see http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1267553811/0
                               // and http://code.google.com/p/digitalwritefast/

volatile float  Motoruino2SMotor::desiredFrequency1;
volatile float  Motoruino2SMotor::desiredFrequency2;

volatile int  Motoruino2SMotor::motorSpeed1;
volatile int  Motoruino2SMotor::motorSpeed2;

volatile signed short  Motoruino2SMotor::counterEncoder1;
volatile signed short  Motoruino2SMotor::counterEncoder2;

volatile float  Motoruino2SMotor::frequency1;
volatile float  Motoruino2SMotor::frequency2;

volatile float Motoruino2SMotor::distance1;
volatile float Motoruino2SMotor::distance2;

volatile unsigned short Motoruino2SMotor::timer1Dec;

unsigned long Motoruino2SMotor::oldMicros_1;
bool Motoruino2SMotor::period1Updated;

unsigned long Motoruino2SMotor::oldMicros_2;
bool Motoruino2SMotor::period2Updated;

Motoruino2SPID Motoruino2SMotor::pid;

volatile float Motoruino2SMotor::perimeter_pertick;
volatile bool Motoruino2SMotor::automatic_motors_control;
volatile short Motoruino2SMotor::reduction_ratio;


Motoruino2SMotor::Motoruino2SMotor()
{
	Config();
}

void Motoruino2SMotor::Config()
{
	mode = SLOW_ENCODER;

	desiredFrequency1 = 0;	desiredFrequency2 = 0;
	motorSpeed1 = 0;		motorSpeed2 = 0;
	counterEncoder1 = 0;	counterEncoder2 = 0;
	frequency1 = 0;			frequency2 = 0;
	distance1 = 0;			distance2 = 0;

	oldMicros_1 = 0; 		oldMicros_2 = 0;
	period1Updated = false;	period2Updated = false;

	perimeter_pertick = 0;

	// Current PWM Values for the motors
	PWM_Value_M1_IN1 = 0;	PWM_Value_M1_IN2 = 0;
	PWM_Value_M2_IN1 = 0;	PWM_Value_M2_IN2 = 0;

	automatic_motors_control = false;

	isEncoderDefined = false;

	timer1Dec = TIMER_RESET;

	pinMode(M1_IN1, OUTPUT);
	pinMode(M1_IN2, OUTPUT);
	pinMode(M2_IN1, OUTPUT);
	pinMode(M2_IN2, OUTPUT);

	analogWrite(M1_IN1, 0);
	analogWrite(M1_IN2, 0);
	analogWrite(M2_IN1, 0);
	analogWrite(M2_IN2, 0);

	PWM_MOTOR_SET(0, 0);

}


// Config Methods
void Motoruino2SMotor::ConfigEncoders(enum EncoderMode _mode, short _reduction_ratio  , float _wheels_diam_cm )
{
	mode = _mode;
	reduction_ratio = _reduction_ratio;
	wheels_diam = _wheels_diam_cm;
	isEncoderDefined = true;

	if (!reduction_ratio) reduction_ratio = 1;

	// Calculate the Perimeter per Encoder TICK divide by reduction
	perimeter_pertick = (PI*(wheels_diam)) / (reduction_ratio) ;


	pinMode(ENC1_INT, INPUT);
	pinMode(ENC2_INT, INPUT);
	pinMode(ENC1_IO, INPUT);
	pinMode(ENC2_IO, INPUT);

	switch (mode)
	{
		case SLOW_ENCODER:

			attachInterrupt(0, InterruptEncoder1_Wheel, CHANGE);
			attachInterrupt(1, InterruptEncoder2_Wheel, CHANGE);

			Timer1.attachInterrupt(InterruptTimer1_Wheel);

			// config Both Wheel PID
			pid.Config(KP, KI, KD);

 			break;

		case FAST_ENCODER:

			attachInterrupt(0, InterruptEncoder1_Motor, RISING);
			attachInterrupt(1, InterruptEncoder2_Motor, RISING);

			//Timer1.initialize(125);							//ms...
			Timer1.attachInterrupt(InterruptTimer1_Motor);
			pid.Config(KP, KI, KD);


			break;

		case HALL_EFFECT_MOTOR:
			attachInterrupt(0, InterruptEncoder1_Motor, RISING);
			attachInterrupt(1, InterruptEncoder2_Motor, RISING);

			//Timer1.initialize(125);							//ms...
			Timer1.attachInterrupt(InterruptTimer1_Motor);
			pid.Config(KP, KI, KD);
			break;

		default:
			break;
	};

	frequency1 = 0;		motorSpeed1 = 0;
	frequency2 = 0;		motorSpeed2 = 0;

	PWM_MOTOR_SET(0, 0);

}

// Methods
long Motoruino2SMotor::GetSpeed(unsigned char motor)
{
	if (motor == 1)
		return frequency1;
	else if (motor == 2)
		return frequency2;
	return 0;
}
void Motoruino2SMotor::SetSpeed(unsigned char motor, long _DesiredFrequency)
{

	automatic_motors_control = true;

	if (motor == 1)
		desiredFrequency1 = _DesiredFrequency;
	else if (motor == 2)
		desiredFrequency2 = _DesiredFrequency;

}

void Motoruino2SMotor::ResetDistance(unsigned char motor)
{
	if (motor == 1)
		distance1 = 0;
	else if (motor == 2)
		distance2 = 0;
}
long Motoruino2SMotor::GetDistance(unsigned char motor)
{
	if (!isEncoderDefined) return 0;

	if (motor == 1)
		return distance1;
	else if (motor == 2)
		return distance2;

	return 0;
}

void Motoruino2SMotor::MOTOR_SET(int speedLeft, int speedRight)
{
	automatic_motors_control = false;

	PWM_MOTOR_SET(speedLeft, speedRight);
}

void Motoruino2SMotor::PWM_MOTOR_SET(int speedLeft, int speedRight)
{
	//Serial.println("M");

	byte _IN1_M1, _IN2_M1, _IN1_M2, _IN2_M2;

	// Sets the correct rotation of the motor
	speedLeft *= M1_DIR;
	speedRight *= M2_DIR;

	// Sets the M2 (Left) Motor
	if (speedLeft < 0)
	{
		_IN1_M2 = 0;
		_IN2_M2 = (byte) speedLeft * -1;
	}
	else
	{
		_IN1_M2 = (byte) speedLeft;
		_IN2_M2 = 0;
	}

	// Sets the M1 (Right) Motor
	if (speedRight < 0)
	{
		_IN1_M1 = 0;
		_IN2_M1 = (byte) speedRight * -1;
	}
	else
	{
		_IN1_M1 = (byte) speedRight;
		_IN2_M1 = 0;
	}

	analogWrite(M1_IN1, _IN1_M1);
	analogWrite(M1_IN2, _IN2_M1);
	analogWrite(M2_IN1, _IN1_M2);
	analogWrite(M2_IN2, _IN2_M2);

}

volatile bool EncoderBSet1;
volatile bool EncoderBSet2;

// ---------------------------------- Interrupts --------------------------------------------

void Motoruino2SMotor::InterruptEncoder1_Motor()
{
	  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
	EncoderBSet1 = digitalReadFast(ENC1_IO);   // read the input pin
	counterEncoder1 -= EncoderBSet1 ? -1 : +1;
}

void Motoruino2SMotor::InterruptEncoder2_Motor()
{
	  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
	EncoderBSet2 = digitalReadFast(ENC2_IO);   // read the input pin
	counterEncoder2 -= EncoderBSet2 ? -1 : +1;
}

void Motoruino2SMotor::InterruptTimer1_Motor()
{

	if (!timer1Dec--)
	{
		timer1Dec = TIMER_RESET;

		// Process Encoder1
		distance1 += counterEncoder1*perimeter_pertick;
		frequency1 = counterEncoder1;
		counterEncoder1=0;
		motorSpeed1 = motorSpeed1 + pid.UpdatePID1((desiredFrequency1) - (int)frequency1);

		if (motorSpeed1 > 254) motorSpeed1 = 254;
		if (motorSpeed1 < -254) motorSpeed1 = -254;

		// Process Encoder2
		distance2 += counterEncoder2*perimeter_pertick;
		frequency2 = counterEncoder2;
		counterEncoder2=0;
		motorSpeed2 = motorSpeed2 + pid.UpdatePID2((desiredFrequency2) - (int)frequency2);

		if (motorSpeed2 > 254) motorSpeed2 = 254;
		if (motorSpeed2 < -254) motorSpeed2 = -254;

		if (automatic_motors_control) PWM_MOTOR_SET(motorSpeed1, motorSpeed2);
	}

}



void Motoruino2SMotor::InterruptEncoder1_Wheel()
{
	unsigned long microsData = micros();
	long Period_1;

	Period_1 =  microsData - oldMicros_1;
	oldMicros_1 = microsData;

	if (Period_1 == 0) Period_1 = 1;

	if (Period_1 > 1000000)
	{
		Period_1 = 1000000;
	}
	else
	{
		Period_1 *= -1*(M1_DIR);

		// Invert Signal if other side (XOR)
		if ((digitalRead(ENC1_INT) == HIGH) && (digitalRead(ENC1_IO) == HIGH))
		{
			Period_1 = -Period_1;
		}
		else if ((digitalRead(ENC1_INT) == LOW) && (digitalRead(ENC1_IO) == LOW))
		{
			Period_1 = -Period_1;
		}
	}

	period1Updated = true;

	frequency1 = (1000000/Period_1);

	// divide by two because it enters the interrupt twice (on change)
	distance1 += perimeter_pertick/2;

	motorSpeed1 = motorSpeed1 + pid.UpdatePID1((desiredFrequency1*2) - (int)frequency1); //multiply f by two because is on change 2 x f...

	if (motorSpeed1 > 254) motorSpeed1 = 254;
	if (motorSpeed1 < -254) motorSpeed1 = -254;

	if (automatic_motors_control) PWM_MOTOR_SET(motorSpeed1, motorSpeed2);

}


void Motoruino2SMotor::InterruptEncoder2_Wheel()
{
	unsigned long microsData = micros();
	long Period_2;

	Period_2 =  microsData - oldMicros_2;
	oldMicros_2 = microsData;


	if (Period_2 == 0) Period_2 = 1;

	if (Period_2 > 1000000)
	{
		Period_2 = 1000000;
	}
	else
	{
		Period_2 *= -1*(M2_DIR);



		// Invert Signal if other side (XOR)
		if ((digitalRead(ENC2_INT) == HIGH) && (digitalRead(ENC2_IO) == HIGH))
		{
			Period_2 = -Period_2;

		}
		else if ((digitalRead(ENC2_INT) == LOW) && (digitalRead(ENC2_IO) == LOW))
		{
			Period_2 = -Period_2;

		}
	}

	period2Updated = true;


	frequency2 = (1000000/Period_2);

	// divide by two because it enters the interrupt twice (on change)
	distance2 += perimeter_pertick/2;

	motorSpeed2 = motorSpeed2 + pid.UpdatePID2((desiredFrequency2*2) - (int)frequency2);

	if (motorSpeed2 > 254) motorSpeed2 = 254;
	if (motorSpeed2 < -254) motorSpeed2 = -254;



	if (automatic_motors_control) PWM_MOTOR_SET(motorSpeed1, motorSpeed2);
}


void Motoruino2SMotor::InterruptTimer1_Wheel()
{

	if (!timer1Dec--)
	{
		timer1Dec = TIMER_RESET;

		if (period1Updated == false)
		{
			// Update with frequency = 0!!
			motorSpeed1 =  motorSpeed1 + pid.UpdatePID1(desiredFrequency1)/2;

			if (motorSpeed1 > 254) motorSpeed1 = 254;
			if (motorSpeed1 < -254) motorSpeed1 = -254;

		} else period1Updated = false;


		if (period2Updated == false)
		{
			// Update with frequency = 0!!
			motorSpeed2 = motorSpeed2 + pid.UpdatePID2(desiredFrequency2)/2;

			if (motorSpeed2 > 254) motorSpeed2 = 254;
			if (motorSpeed2 < -254) motorSpeed2 = -254;

		} else period2Updated = false;

		if (automatic_motors_control) PWM_MOTOR_SET(motorSpeed1, motorSpeed2);
	}

}

// ---------------------------------- PID Controller --------------------------------------------


Motoruino2SPID::Motoruino2SPID()
{
	Config(KP,KD,KI);
}


void Motoruino2SPID::Config(float _Kp, float _Kd, float _Ki)
{
	SetupPID1(_Kp, _Kd , _Ki);
	SetupPID2(_Kp, _Kd , _Ki);

	pidFirstTime1 = true;
	pidFirstTime2 = true;
}

void Motoruino2SPID::SetupPID1(float factorProportional, float factorDerivative, float factorIntegrative)
{
  pidFirstTime1 = true;
  pidSumCTE1 =0;
  factorP1 = factorProportional;
  factorD1 = factorDerivative;
  factorI1 = factorIntegrative;
  pidRetValue1 =0;
}

float Motoruino2SPID::UpdatePID1(float cte)
{

    if(pidFirstTime1)
    {
      pidFirstTime1 = false;
      pidPrevCTE1 = cte;
    }

    float proportional = cte * factorP1;

    float d = pidPrevCTE1 - cte;

    float derivative = d * factorD1;

    pidSumCTE1 += cte;
    float integrative = pidSumCTE1 * factorI1;

    pidRetValue1 = proportional + derivative + integrative;


  return pidRetValue1;
}

void Motoruino2SPID::SetupPID2(float factorProportional, float factorDerivative, float factorIntegrative)
{
  pidFirstTime2 = true;
  pidSumCTE2 =0;
  factorP2 = factorProportional;
  factorD2 = factorDerivative;
  factorI2 = factorIntegrative;
  pidRetValue2 =0;
}

float Motoruino2SPID::UpdatePID2(float cte)
{

    if(pidFirstTime2)
    {
      pidFirstTime2 = false;
      pidPrevCTE2 = cte;
    }

    float proportional = cte * factorP2;
    float d = pidPrevCTE2 - cte;
    float derivative = d * factorD2;

    pidSumCTE2 += cte;
    float integrative = pidSumCTE2 * factorI2;

    pidRetValue2 = proportional + derivative + integrative;


  return pidRetValue2;
}



