#include "Arduino.h"

#include "Metro.h"
#include "Wire.h"
#include "TimerOne.h"

#include "LSM330DLC.h"
#include "Motoruino2SGPOut.h"
#include "Motoruino2SMotor.h"
#include "Motoruino2SPower.h"
#include "Motoruino2SSPI.h"


// ----- DEFINES -----
#define BLINKTIME 			250
#define NAVIGATIONTIME 		10

#define ANGLE_OK 100			// Angle x 10 => example: 10deg = 1000 or 1deg = 100
#define ANGLE_MEDIUM 8000

#define BUFFER_SIZE 	10
#define WIRE_ADDRESS 	2

// ----- OBJECTS/GLOBAL -----
LSM330DLC 			onboardIMU;	// IMU
Motoruino2SGPOut 	gpOut;		// General Purpose Output
Motoruino2SMotor 	motor;		// Motor Functions
Motoruino2SPower 	power;		// Power Functions
Motoruino2SSPI		spi;		// SPI Functions

// Metro Update
Metro MetroUpdateTime = Metro(BLINKTIME);
Metro MetroNavigationTime = Metro(NAVIGATIONTIME);

// Wire Comm
byte receivedBuffer[BUFFER_SIZE];
byte sendBuffer[BUFFER_SIZE];
unsigned char sendLength = 0;

// Comm Protocol
enum _ReplyOrders { NO_REPLY , REPLY_CALIBRATE_IMU, REPLY_GET_HEADING, REPLY_GOTO_DEGREES};
enum _ReplyOrders ProcessLaterOrder = NO_REPLY;

// Pooling Update Flags
bool startFlagIMU = false; 				// Get IMU Values?
bool startNavigateToAngle = false;	// Goto Angle?
bool readyFlagToReply = false;			// Finished processing the reply?

// Navigation
int goalAngle = 0;
byte goalSpeed = 0;

// Aux Global Vars
long Data = 0;

// ----- SETUP -----
void setup() {

	Serial.begin(115200);

	// Object Configurations
	//gpOut.Config();
	//motor.Config();
	//power.Config();
	spi.config();
	onboardIMU.Config();

	// Comm Configuration
	//CommConfig();

	// Pools/Flags init
	startFlagIMU = true;
	startNavigateToAngle = false;
	readyFlagToReply = false;

	// Main Values init
	goalAngle = 0;
	goalSpeed = 0;

	// Aux Values init
	Data = 0;
}

// ----- LOOP -----
void loop() {

	// POOLING Functions
	// - IMU
	if (startFlagIMU) onboardIMU.UpdateGyro();		// Gyro Pooling function
//
	// - Navigation
//	if (StartNavigateToAngle)					// Navigation Pooling function
//		if (MetroNavigationTime.check() == 1)
//			if (ReachAngle((long) GoalAngle, GoalSpeed)) StartNavigateToAngle = false;

	// - Wire Communications (Master)
	//UpdateComm();

	// TEST Functions to print
	if (MetroUpdateTime.check() == 1)			// Test Stuff
	{
		Serial.print(onboardIMU.GetHeading()) ; //Serial.print('\t');
		Serial.println("");


	}

}





