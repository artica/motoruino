#include "LSM330DLC.h"
#include "Motoruino2SSPI.h"


unsigned long millis(void);

Metro MetroGUpdateValues = Metro(SAMPLE_G_TIME);
Metro MetroAUpdateValues = Metro(SAMPLE_A_TIME);

//Motoruino2SSPI LSM330DLC::spi;

// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor
LSM330DLC::LSM330DLC()
{
	InitializationEnded = 0;
}

void LSM330DLC::Config()
{
	unsigned long start = 0;

	Serial.begin(115200);

	// SPI Chip-Select Setup
	_CS_GYRO = CS_GYRO;
	_CS_ACCEL = CS_ACCEL;
    pinMode(_CS_ACCEL, OUTPUT);
    pinMode(_CS_GYRO,  OUTPUT);
    digitalWrite(_CS_ACCEL, HIGH);
    digitalWrite(_CS_GYRO,  HIGH);

    StreamGyroPointer = 0;

    // SPI Config
    spi.config();

    //Delay before start 50ms
    start = millis();
    while (millis() - start < 50);

    // Accelerometer
    writeReg(_CS_ACCEL, LSM330D_CTRL_REG1_A, 0b10010111);
    writeReg(_CS_ACCEL, LSM330D_CTRL_REG2_A, 0b00000000);
    writeReg(_CS_ACCEL, LSM330D_CTRL_REG3_A, 0b00001000);
    writeReg(_CS_ACCEL, LSM330D_CTRL_REG4_A, 0b00001000);    // +/-2G (1mg/digt)
    writeReg(_CS_ACCEL, LSM330D_CTRL_REG5_A, 0b01000000);

    // Gyro
    writeReg(_CS_GYRO, LSM330D_CTRL_REG1_G, 0b11111111);
    writeReg(_CS_GYRO, LSM330D_CTRL_REG2_G, 0b00000111);
    writeReg(_CS_GYRO, LSM330D_CTRL_REG3_G, 0b00000000);
    writeReg(_CS_GYRO, LSM330D_CTRL_REG4_G, 0b00110000);     //2000dps // 250dps (8.75mdps/digit) //500dps s
    writeReg(_CS_GYRO, LSM330D_CTRL_REG5_G, 0b00000000);
    writeReg(_CS_GYRO, LSM330D_FIFO_CTRL_REG_G, 0b01011111);


    InitializationEnded = 1;
}

// ---------------------------------------------------------------------
// Function in the Calibration Fase
int LSM330DLC::CalibrateGyro()
{
	unsigned long start =  millis();

	// DUMY SAMPLES
	CalibrationSamples = CALIB_SAMPLES;
	start = millis();
	while ((CalibrationSamples) && ((millis() - start < 20000)))
	{
    	this->UpdateGyro();
	}

	RollError = 0;	PitchError = 0;	HeadingError = 0;
	Roll_x = 0;		Pitch_y = 0;	Heading_z = 0;

	// Real Samples
	CalibrationSamples = CALIB_SAMPLES;
	start = millis();
	while ((CalibrationSamples) && ((millis() - start < 20000)))
	{
    	this->UpdateGyro();
	}



	// If not exit by timeout
	if (!CalibrationSamples)
	{
		RollError = (Roll_x / (CALIB_SAMPLES));
		PitchError = (Pitch_y / (CALIB_SAMPLES));
		HeadingError = (Heading_z / (CALIB_SAMPLES));
		Roll_x = 0;
		Pitch_y = 0;
		Heading_z = 0;
		return 1;
	} else {
		Roll_x = 0;
		Pitch_y = 0;
		Heading_z = 0;
		return 0;
	}

//	Serial.println("ERROR");
//	Serial.println(RollError);
//	Serial.println(Roll_x);
//	Serial.println(PitchError);
//	Serial.println(Pitch_y);
//	Serial.println(HeadingError);
//	Serial.println(Heading_z);

}

byte LSM330DLC::GetWHO_AM_I_G()
{
	readReg(_CS_GYRO, LSM330D_WHO_AM_I_G, 1, BufferSPI);
	return BufferSPI[0];
}

// ---------------------------------------------------------------------
// Function called by pooling on the main function
void LSM330DLC::UpdateGyro()
{
	if (!InitializationEnded) return;

	// ** GET GYRO VALUES -> Test if "new" GYRO Value @ IMU SAMPLE TIME FREQUENCY **
	readReg(_CS_GYRO, LSM330D_STATUS_REG_G, 1, BufferSPI);
	if (BufferSPI[0] && 0b00000100)
	{
		// Get Gyro
		readReg(_CS_GYRO, LSM330D_OUT_X_L_G, 6, BufferSPI);
		GyroRaw_x = ((BufferSPI[1] << 8) | BufferSPI[0]);
		GyroRaw_y = ((BufferSPI[3] << 8) | BufferSPI[2]);
		GyroRaw_z = ((BufferSPI[5] << 8) | BufferSPI[4]);

		StreamGyroX[StreamGyroPointer] = GyroRaw_x;
		StreamGyroY[StreamGyroPointer] = GyroRaw_y;
		StreamGyroZ[StreamGyroPointer++] = GyroRaw_z;

		// Reached the end without update the Values
		// -> Record only the last Value and Discard All Others
		if (StreamGyroPointer ==  ODR_G)
		{
			Serial.println("NO_GYRO");

			StreamGyroPointer = 1;
			StreamGyroX[0] = GyroRaw_x;
			StreamGyroY[0] = GyroRaw_y;
			StreamGyroZ[0] = GyroRaw_z;
		}
	}

	// ** PROCESS GYRO VALUES Process Buffer Recorded Data @ uC SAMPLE TIME FREQUENCY **
	if (MetroGUpdateValues.check() == 1)
	{


		// Add all elements of the recorded Stream
		TempDataX = 0; TempDataY = 0; TempDataZ = 0;
		for (int i = StreamGyroPointer + 1; i--; )
		{
			TempDataX += StreamGyroX[i];
			TempDataY += StreamGyroY[i];
			TempDataZ += StreamGyroZ[i];
		}

		// Divide by the Steam and update the Heading, Pitch and Roll
		TempDataX = TempDataX / StreamGyroPointer;
		TempDataY = TempDataY / StreamGyroPointer;
		TempDataZ = TempDataZ / StreamGyroPointer;

		Roll_x += (TempDataX * 0.92) - RollError;
		Pitch_y += (TempDataY * 0.92) - PitchError;
		Heading_z += (TempDataZ * 0.92) - HeadingError;

		if (Roll_x > 50400)
			Roll_x =  Roll_x - (long)100800;
		else if (Roll_x < -50400)
			Roll_x =  Roll_x + (long)100800;

		if (Pitch_y > 50400)
			Pitch_y = Pitch_y - (long)100800;
		else if (Pitch_y < -50400)
			Pitch_y =  Pitch_y + (long)100800;

		if (Heading_z > 50400)
			Heading_z =  Heading_z - (long)100800;
		else if (Heading_z < -50400)
				Heading_z =  Heading_z + (long)100800;

		Heading_z_Angle = (long)((Heading_z * 180)/(long)504);
		Pitch_y_Angle = (long)((Pitch_y * 180)/(long)504);
		Roll_x_Angle = (long)((Roll_x * 180)/(long)504);

		StreamGyroPointer = 0;

		CalibrationSamples--;

    }
}

// Get the current heading, pitch and roll for motoruino (in degrees)
long LSM330DLC::GetHeading(){
	return Heading_z_Angle;
}
long LSM330DLC::GetPitch(){		return Pitch_y_Angle;}
long LSM330DLC::GetRoll(){		return Roll_x_Angle;}

// Resets the current heading referential, making the current heading = 0 degrees
void LSM330DLC::ResetGyro()
{
	Roll_x = 0;
	Pitch_y = 0;
	Heading_z = 0;

}

// Resets the current heading referential, making the current heading = 0 degrees
void LSM330DLC::ResetHeading()
{

	Heading_z = 0;

}


// Get the raw values from the accelerometer and gyroscope for the different axis
int LSM330DLC::GetAccelerometerX(){	return AccelRaw_x;}
int LSM330DLC::GetAccelerometerY(){	return AccelRaw_y;}
int LSM330DLC::GetAccelerometerZ(){	return AccelRaw_z;}

int LSM330DLC::GetGyroscoperX(){	return GyroRaw_x;}
int LSM330DLC::GetGyroscoperY(){	return GyroRaw_y;}
int LSM330DLC::GetGyroscoperZ(){	return GyroRaw_z;}

// Aux functions for the SPI Comm to the Chip
void LSM330DLC::writeReg(int cs_pin, byte reg, byte value)
{
    reg = reg & 0x7F;               // bit7-0 (write)
    reg = reg & 0xBF;               // bit6-0 (address not-inc)

    digitalWrite(cs_pin, LOW);
    spi.transfer(reg);
    spi.transfer(value);
    digitalWrite(cs_pin, HIGH);
}


void LSM330DLC::readReg(byte cs_pin, byte reg, byte len, byte *data)
{
	unsigned char i;

    reg = reg | 0x80;               // bit7-1 (read)
    reg = reg | 0x40;               // bit6-1 (address auto-inc)

    digitalWrite(cs_pin, LOW);
    Motoruino2SSPI::transfer(reg);

    for (i = len; i--; ){
        *data = spi.transfer(0x00);
        data ++;
    }

    digitalWrite(cs_pin, HIGH);
}


