
#ifndef Motoruino2SGPOut_h
#define Motoruino2SGPOut_h

#include "Arduino.h"

#define GP_OUT 		0		// GPO 1Amp Generic Output

class Motoruino2SGPOut
{

	public:
		// Constructor
		Motoruino2SGPOut();

		// Config
		void Config();


	private:

};

#endif
