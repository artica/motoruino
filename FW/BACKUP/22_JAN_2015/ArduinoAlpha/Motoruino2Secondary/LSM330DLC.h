//Motor Control

#ifndef LSM330DLC_h
#define LSM330DLC_h

#include "Arduino.h"
#include <Metro.h>
#include "Motoruino2SSPI.h"

#define CS_ACCEL 8
#define CS_GYRO  4 // A6

#define GPIO A1

#define MAX_SPI_BUFFER 6

#define CALIB_SAMPLES 100

#define SAMPLE_G_TIME 50		// 20Hz-> @ SAMPLE_TIME = 50ms
#define ODR_G 76					// Gyro "Output Data Rate" Setup = 760Hz -> 76

#define SAMPLE_A_TIME 50		// 20Hz-> @ SAMPLE_TIME = 50ms
#define ODR_A 76

// LSM330D Register Map
// Accelerometer
#define LSM330D_CTRL_REG1_A     0x20
#define LSM330D_CTRL_REG2_A     0x21
#define LSM330D_CTRL_REG3_A     0x22
#define LSM330D_CTRL_REG4_A     0x23
#define LSM330D_CTRL_REG5_A     0x24

#define LSM330D_OUT_X_L_A       0x28
#define LSM330D_OUT_X_H_A       0x29
#define LSM330D_OUT_Y_L_A       0x2A
#define LSM330D_OUT_Y_H_A       0x2B
#define LSM330D_OUT_Z_L_A       0x2C
#define LSM330D_OUT_Z_H_A       0x2D


// Gyroscope
#define LSM330D_WHO_AM_I_G      0x0F
#define LSM330D_WHOAMI_G_VAL    0xD4

#define LSM330D_CTRL_REG1_G     0x20
#define LSM330D_CTRL_REG2_G     0x21
#define LSM330D_CTRL_REG3_G     0x22
#define LSM330D_CTRL_REG4_G     0x23
#define LSM330D_CTRL_REG5_G     0x24

#define LSM330D_OUT_X_L_G       0x28
#define LSM330D_OUT_X_H_G       0x29
#define LSM330D_OUT_Y_L_G       0x2A
#define LSM330D_OUT_Y_H_G       0x2B
#define LSM330D_OUT_Z_L_G       0x2C
#define LSM330D_OUT_Z_H_G       0x2D

#define LSM330D_STATUS_REG_G    0x27
#define LSM330D_FIFO_CTRL_REG_G   0x2E




 

class LSM330DLC
{
	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration
		// Default constructor
		LSM330DLC();

		void Config();

		// ---------------------------------------------------------------------
		// Gyro Data
		// Get the current heading for motoruino (in degrees)
		long GetHeading();
		// Get the current heading for motoruino (in degrees)
		long GetPitch();
		// Get the current heading for motoruino (in degrees)
		long GetRoll();

		// Resets the current heading referential, making the current heading = 0 degrees
		void ResetGyro();



		void ResetHeading();

		// Get the raw values from the accelerometer and gyroscope for the different axis
		int GetAccelerometerX();
		int GetAccelerometerY();
		int GetAccelerometerZ();

		int GetGyroscoperX();
		int GetGyroscoperY();
		int GetGyroscoperZ();

		// Calibrate Gyroscope Result: "1" if OK or "0" if NOK;
		int CalibrateGyro();
		byte GetWHO_AM_I_G();
		// pooling function for Gyroscope Data Update
		void UpdateGyro();

		// pooling function for Accelerometer Data Update
		// @TODO

	private:

		Motoruino2SSPI spi;

		int _CS_GYRO;
		int _CS_ACCEL;

		// Gyroscope Stream Memory
		short StreamGyroX[ODR_G];
		short StreamGyroY[ODR_G];
		short StreamGyroZ[ODR_G];
		unsigned char StreamGyroPointer;

		// Accelerometer Stream Memory
		short StreamAcelX[ODR_A];
		short StreamAcelY[ODR_A];
		short StreamAcelZ[ODR_A];
		unsigned char StreamAcelPointer;


		// Gyroscope processed angle - "were it is turned in Raw Values"
		volatile long Roll_x;
		volatile long Pitch_y;
		volatile long Heading_z;

		volatile long Roll_x_Angle;
		volatile long Pitch_y_Angle;
		volatile long Heading_z_Angle;

		short RollError;
		short PitchError;
		short HeadingError;

		// Raw Values Received
		short AccelRaw_x, AccelRaw_y, AccelRaw_z;
		short GyroRaw_x,  GyroRaw_y,  GyroRaw_z;

		int debug_var;
		volatile int CalibrationSamples;

		long TempDataX;
		long TempDataY;
		long TempDataZ;

		byte BufferSPI[MAX_SPI_BUFFER];

		unsigned char InitializationEnded;

		// SPI Functions to read and Write
		void writeReg(int cs_pin, byte reg, byte value);
		void readReg(byte cs_pin, byte reg, byte len, byte *data);
};
#endif
