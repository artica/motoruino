/*
 * GyroPower.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#ifndef Motoruino2SPower_h
#define Motoruino2SPower_h

#include "Arduino.h"

#define SLEEP_IN 	3		// Input Pin for Sleep

class Motoruino2SPower
{

	public:
		// Constructor
		Motoruino2SPower();

		// Config
		void Config();


	private:

};

#endif


