#include "GyroBumper.h"
 
// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor, uses pins FARRUSCO_DEFAULT_BUMPER_LEFT_PIN and 
// FARRUSCO_DEFAULT_BUMPER_RIGHT_PIN as default		
GyroBumper::GyroBumper()
{
	configure(_DEFAULT_BUMPER_LEFT_PIN, _DEFAULT_BUMPER_RIGHT_PIN);
}

// Custom constructor, specify the pins for both bumpers
GyroBumper::GyroBumper(int pinBumperLeft, int pinBumperRight)
{
	configure(pinBumperLeft, pinBumperRight);
}

// Configure the pins for the front bumpers
void GyroBumper::Begin(int pinBumperLeft, int pinBumperRight)
{	
	configure(pinBumperLeft, pinBumperRight);
	initialize();
}

// Configure the necessary values to initialize the BumperSensors class
void GyroBumper::configure( int pinBumperLeft, int pinBumperRight )
{
	_initialized = false;
	// Base configuration for the bumpers
	_pinBumperLeft = pinBumperLeft;
	_pinBumperRight = pinBumperRight;	
}

// Initializes all the necessary variables and pins so they can be used properly
void GyroBumper::initialize()
{	
	// declare frontal Bumpers as inputs with the pull-up resistor active
	pinMode(_pinBumperLeft, INPUT_PULLUP);
	pinMode(_pinBumperRight, INPUT_PULLUP);
	_initialized = true;	
}

//Returns left bumper value. (active = TRUE)
bool GyroBumper::CheckLeftBumper()
{
	// Make sure everything is initialized before trying to read data 
	if (!_initialized) initialize();
	return( digitalRead(_pinBumperLeft) );
}

//Returns right bumper value. (active = TRUE)
bool GyroBumper::CheckRightBumper()
{
	// Make sure everything is initialized before trying to read data 
	if (!_initialized) initialize();
	return( digitalRead(_pinBumperRight) );
}

