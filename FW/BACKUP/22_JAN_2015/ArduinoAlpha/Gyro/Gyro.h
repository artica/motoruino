#ifndef Gyro_h
#define Gyro_h

#include "Arduino.h"

#include "GyroBumper.h"
#include "GyroIRSensor.h"
#include "GyroServoControl.h"
#include "GyroSerialCommand.h"
#include "Motoruino2Main.h"

class Gyro
{ 
	public:
		// Default constructor
		// This creates a Gyro object with the default configuration
		Gyro();

		// Different components that form a Gyrp
		GyroBumper bumpers;
		Motoruino2Main motoruino2;
		GyroIRSensor distanceSensor;
		GyroServoControl neck;
};

#endif
