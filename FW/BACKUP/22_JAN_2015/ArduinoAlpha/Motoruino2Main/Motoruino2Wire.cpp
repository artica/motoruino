#include "Motoruino2Wire.h"


// ---------------------------------------------------------------------
// Initialization and configuration

// Default constructor, initializes the motors in the default pins (3,5,6,11)
WireComm::WireComm()
{
	_initialized = false;
}

void WireComm::begin()
{
	if (_initialized) return;



	// Flag revealing that all configurations have ended - Last thing to do here
	_initialized = true;
}

// ---------------------------------------------------------------------
// SendFunction
byte* WireComm::sendData(byte* array, byte Size, unsigned short replyDelay, byte ReplySize)
{
	int i = 0;
	long start = 0;

	Wire.begin();
	Wire.beginTransmission(SLAVE_ADD);

	for (int x = 0; x < Size; x++ )
	{
		Wire.write(array[x]);
	}

	Wire.endTransmission();    // stop transmitting

	delay(3);
	delay(replyDelay);
	// request ReplySize bytes from slave device #SLAVE_ADD
	if (ReplySize){

		Wire.requestFrom(SLAVE_ADD, (int) ReplySize);

		// Wait for Reply
		start = millis();
		while (Wire.available() && (millis() - start < 10000))    // slave may send less than requested
		{
			ReplyBuffer[i++] = Wire.read(); // receive a byte as character
		}

	}

	return ReplyBuffer;

}


