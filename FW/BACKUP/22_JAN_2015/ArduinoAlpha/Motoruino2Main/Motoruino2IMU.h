//Motor Control

#ifndef IMUSensor_h
#define IMUSensor_h

#include "Arduino.h"

#include <Metro.h>
#include "Motoruino2Wire.h"
 
class Motoruino2IMU
{
	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration
		// Default constructor
		Motoruino2IMU();

		bool StartGyro();
		bool StopGyro();

		bool CalibrateGyro();

		// ---------------------------------------------------------------------
		// Get the current heading for Gyro (in degrees)
		long GetHeading();
		long GetPitch();
		long GetRoll();

		// Resets the current heading referential, making the current heading = 0 degrees
		void ResetGyroscope();

		// Get the raw values from the accelerometer and gyroscope for the different axis
		short getAccelerometerX();
		short getAccelerometerY();
		short getAccelerometerZ();

		short getGyroscopeX();
		short getGyroscopeY();
		short getGyroscopeZ();

	private:
		int debug_var;
		bool _initialized;

		long GetLongValue(byte Buff[8]);
		short GetShortValue(byte Buff[6]);

		WireComm Comm;
};
#endif
