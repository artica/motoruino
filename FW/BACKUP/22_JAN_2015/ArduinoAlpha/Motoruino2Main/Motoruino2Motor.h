//Motor Control

#ifndef MotorControl_h
#define MotorControl_h
#include "Arduino.h"
#include "Motoruino2Wire.h"

// Define the rotation (chose between 1 or -1)
#define M1_SIGNAL -1
#define M2_SIGNAL 1

 
class Motoruino2Motor
{
	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration
		// Default constructor, initializes the motors in the default pins (3,5,6,11) 
		Motoruino2Motor();


		// ---------------------------------------------------------------------
		// Movement methods
		// Stops the movement completely
		void StopMovement();
		// Not specifying any speed will make Farrusco move at maximum speed
		// Moves straight forward
		void MoveForward (unsigned int distance = 0, byte speed = 255);
		// Moves straight backward
		void MoveBackward (unsigned int distance = 0, byte speed = 255);
		// Turning rotates the wheels in opposite directions in order to change the heading
		void TurnLeft (int angle = 0, byte speed = 255);
		void TurnRight (int angle = 0, byte speed = 255);
		// Assign the speed and direction for both motors directly
		// The speed values are in the range [ -255, 255 ]
		void MoveMotors( int speedLeft, int speedRight );

		// Moves forward while turning on a specific angle, in degrees
		// With angle = 0 degrees the movement is straight forward
		void MoveForwardArc (int angularVelocity, unsigned int distance = 0, byte speed = 255);
		// Farrusco turns in order to face a specific heading
		// The rotation direction is chosen in order to minimize the rotation
		bool TurnTo (int angle, byte speed = 255);

		// Sets the H Bridge PWM Values Independently - Can be used for other applications example as a LED Driver
		void HBridgePWMSet(byte IN1_M1, byte IN2_M1, byte IN1_M2, byte IN2_M2);

	private:

		WireComm Comm;

		// Is the sensor initialized?
		// The initialization occurs when Begin is called, or when it is used the first time
		bool _initialized;
};
#endif
