//Motoruino Board

#ifndef Motoruino_h
#define Motoruino_h

#include "Arduino.h"
#include "Motoruino2IMU.h"
#include "Motoruino2Motor.h"

#define SLEEP 12
#define BUZZ 11

#define BATTERY AD5

class Motoruino2Main
{

	public:
		// ---------------------------------------------------------------------
		// Initialization and configuration

		// Default constructor
		Motoruino2Main();

		// ---------------------------------------------------------------------
		// Methods
		void begin();

		// ---------------------------------------------------------------------
		// Objects


		Motoruino2Motor motors;
		Motoruino2IMU imu;

	private:

};

#endif

