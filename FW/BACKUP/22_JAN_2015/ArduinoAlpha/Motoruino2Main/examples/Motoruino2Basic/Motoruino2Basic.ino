/*  ----------------------------------------------------------------------------
 *  Motoruino2 Basic Example
 *  ----------------------------------------------------------------------------
 * 	by Artica CC
 * 	BS and TM
 *
 * Rev0. August 2014 - Project Startup
 *
 * BOARD: Arduino Leonardo
*/

#include "Arduino.h"
#include "Wire.h"
#include <Metro.h>
#include <Servo.h>
#include <Motoruino2Main.h>


#define LED13 13

Motoruino2Main board;

long longvar;
long start_timeout;

int Side = 1;

// the setup routine runs once when you press reset:
void setup() {    

	// Config the BaudRate for Debug USB Port and Comm Port (Serial1)
	Serial.begin(115200);
	Serial1.begin(9600);

	// Define the LED BLINK STATE
	pinMode(LED13, OUTPUT);

	// Timeout initialization var
	start_timeout = 0;

	// Launch Motoruino2 Configurations
	board.begin();

	// Wait
	delay(100);

	// Calibrate the Gyroscope for correct readings, and wait for reply
	digitalWrite(LED13, LOW);
	if (board.imu.CalibrateGyro())
		Serial1.println("Calibrated OK");
	else
		Serial1.println("Not Calibrated");
	digitalWrite(LED13, HIGH);

	// Wait
	delay(1000);

}

// the loop routine runs over and over again forever:
void loop() {

  delay(100);               // wait for a second

  Serial.println(board.imu.GetHeading());

//	Motoruino.Motors.TurnTo( 0, 255);

//	start_timeout = millis();
//	while (millis() - start_timeout < 2000)
//	{
//		Motoruino.Motors.TurnTo( 0, 255);
//		delay(10);
//		Serial.println(Motoruino.IMU.GetHeading());
//	}

//	start_timeout = millis();
//	while (millis() - start_timeout < 2000)
//	{
//		Motoruino.Motors.TurnTo( Side * -9000, 255);
//		delay(10);
//		Serial.println(Motoruino.IMU.GetHeading());
//	}
//
//
//	start_timeout = millis();
//	while (millis() - start_timeout < 2000)
//	{
//		Motoruino.Motors.TurnTo( 18000, 255);
//		delay(10);
//		Serial.println(Motoruino.IMU.GetHeading());
//	}
//
//
//	start_timeout = millis();
//	while (millis() - start_timeout < 2000)
//	{
//		Motoruino.Motors.TurnTo( Side * 9000, 255);
//		delay(10);
//		Serial.println(Motoruino.IMU.GetHeading());
//	}


}
