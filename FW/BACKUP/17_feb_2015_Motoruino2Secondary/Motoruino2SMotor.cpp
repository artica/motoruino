/*
 * GyroMotor.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#include "Arduino.h"
#include "Motoruino2SMotor.h"
#include "TimerOne.h"

volatile float  Motoruino2SMotor::DesiredFrequency1;
volatile float  Motoruino2SMotor::DesiredFrequency2;

volatile int  Motoruino2SMotor::MotorSpeed1;
volatile int  Motoruino2SMotor::MotorSpeed2;

volatile unsigned long  Motoruino2SMotor::counterEncoder1;
volatile unsigned long  Motoruino2SMotor::counterEncoder2;

volatile float  Motoruino2SMotor::Frequency1;
volatile float  Motoruino2SMotor::Frequency2;

volatile float Motoruino2SMotor::Distance1;
volatile float Motoruino2SMotor::Distance2;

volatile unsigned short Motoruino2SMotor::Timer1Dec;

unsigned long Motoruino2SMotor::oldMicros_1;
bool Motoruino2SMotor::Period_1_updated;
unsigned long Motoruino2SMotor::oldMicros_2;
bool Motoruino2SMotor::Period_2_updated;

Motoruino2SPID Motoruino2SMotor::pid;

volatile float Motoruino2SMotor::perimeter;
volatile bool Motoruino2SMotor::automatic_motors_control;


Motoruino2SMotor::Motoruino2SMotor()
{
	Config();
}

void Motoruino2SMotor::Config()
{
	mode = SLOW_ENCODER;

	DesiredFrequency1 = 0;
	DesiredFrequency2 = 0;

	MotorSpeed1 = 0;
	MotorSpeed2 = 0;

	counterEncoder1 = 0;
	counterEncoder2 = 0;

	Frequency1 = 0;
	Frequency2 = 0;

	Distance1 = 0;
	Distance2 = 0;

	oldMicros_1 = 0;
	Period_1_updated = false;

	oldMicros_2 = 0;
	Period_2_updated = false;

	// Current PWM Values for the motors
	PWM_Value_M1_IN1 = 0;
	PWM_Value_M1_IN2 = 0;
	PWM_Value_M2_IN1 = 0;
	PWM_Value_M2_IN2 = 0;

	automatic_motors_control = false;

	isEncoderDefined = false;

	Timer1Dec = TIMER_RESET;

	pinMode(M1_IN1, OUTPUT);
	pinMode(M1_IN2, OUTPUT);
	pinMode(M2_IN1, OUTPUT);
	pinMode(M2_IN2, OUTPUT);

	analogWrite(M1_IN1, 0);
	analogWrite(M1_IN2, 0);
	analogWrite(M2_IN1, 0);
	analogWrite(M2_IN2, 0);

	PWM_MOTOR_SET(0, 0);

}


// Config Methods
void Motoruino2SMotor::ConfigEncoders(enum EncoderMode _mode, short _reduction_ratio  , float _wheels_diam_cm )
{
	mode = _mode;
	reduction_ratio = _reduction_ratio;
	wheels_diam = _wheels_diam_cm;
	isEncoderDefined = true;

	if (!reduction_ratio) reduction_ratio = 1;

	// reduction_Ratio * 2 because of the frequency
	perimeter = (2*PI*(wheels_diam/2)) / (reduction_ratio) ;



	pinMode(ENC1_INT, INPUT);
	pinMode(ENC2_INT, INPUT);
	pinMode(ENC1_IO, INPUT);
	pinMode(ENC2_IO, INPUT);

	switch (mode)
	{
		case SLOW_ENCODER:

			attachInterrupt(0, InterruptEncoder1_Wheel, CHANGE);
			attachInterrupt(1, InterruptEncoder2_Wheel, CHANGE);

			Timer1.attachInterrupt(InterruptTimer1_Wheel);

			Serial.print("PER TIK:"); Serial.println(perimeter);

			// config Both Wheel PID
			pid.Config(KP, KI, KD);

			break;

		case FAST_ENCODER:

			attachInterrupt(0, InterruptEncoder1_Motor, CHANGE);
			attachInterrupt(1, InterruptEncoder2_Motor, CHANGE);

			Timer1.initialize(125);							//ms...
			Timer1.attachInterrupt(InterruptTimer1_Motor);
			break;

		case HALL_EFFECT_MOTOR:
			break;

		default:
			break;
	};



	Frequency1 = 0;
	MotorSpeed1 = 0;
	Frequency2 = 0;
	MotorSpeed2 = 0;

	PWM_MOTOR_SET(0, 0);

}

// Methods
long Motoruino2SMotor::GetSpeed(unsigned char motor, long _DesiredFrequency)
{
	if (motor == 1)
		return Frequency1;
	else if (motor == 2)
		return Frequency2;
	return 0;
}
void Motoruino2SMotor::SetSpeed(unsigned char motor, long _DesiredFrequency)
{

	automatic_motors_control = true;

	if (motor == 1)
		DesiredFrequency1 = _DesiredFrequency;
	else if (motor == 2)
		DesiredFrequency2 = _DesiredFrequency;

}

void Motoruino2SMotor::ResetDistance(unsigned char motor)
{
	if (motor == 1)
		Distance1 = 0;
	else if (motor == 2)
		Distance2 = 0;
}
long Motoruino2SMotor::GetDistance(unsigned char motor)
{
	if (!isEncoderDefined) return 0;

	if (motor == 1)
		return Distance1;
	else if (motor == 2)
		return Distance2;
}

void Motoruino2SMotor::MOTOR_SET(int speedLeft, int speedRight)
{
	automatic_motors_control = false;

	PWM_MOTOR_SET(speedLeft, speedRight);
}

void Motoruino2SMotor::PWM_MOTOR_SET(int speedLeft, int speedRight)
{
	//Serial.println("M");

	byte _IN1_M1, _IN2_M1, _IN1_M2, _IN2_M2;

	// Sets the correct rotation of the motor
	speedLeft *= M1_DIR;
	speedRight *= M2_DIR;

	// Sets the M2 (Left) Motor
	if (speedLeft < 0)
	{
		_IN1_M2 = 0;
		_IN2_M2 = (byte) speedLeft * -1;
	}
	else
	{
		_IN1_M2 = (byte) speedLeft;
		_IN2_M2 = 0;
	}

	// Sets the M1 (Right) Motor
	if (speedRight < 0)
	{
		_IN1_M1 = 0;
		_IN2_M1 = (byte) speedRight * -1;
	}
	else
	{
		_IN1_M1 = (byte) speedRight;
		_IN2_M1 = 0;
	}

	analogWrite(M1_IN1, _IN1_M1);
	analogWrite(M1_IN2, _IN2_M1);
	analogWrite(M2_IN1, _IN1_M2);
	analogWrite(M2_IN2, _IN2_M2);

}


// ---------------------------------- Interrupts --------------------------------------------

void Motoruino2SMotor::InterruptEncoder1_Motor()
{
	if (digitalRead(ENC1_IO))
		counterEncoder1++;
	else
		counterEncoder1--;
}
void Motoruino2SMotor::InterruptEncoder2_Motor()
{
	if (digitalRead(ENC2_IO))
		counterEncoder2++;
	else
		counterEncoder2--;
}
void Motoruino2SMotor::InterruptTimer1_Motor()
{
	// Process Encoder1
	Frequency1 = (Frequency1 * 0.5) + ((counterEncoder1/TIMER_RACIO) * 0.5);

	// Process Encoder2
	Frequency2 = (Frequency2 * 0.5) + ((counterEncoder2/TIMER_RACIO) * 0.5);

}



void Motoruino2SMotor::InterruptEncoder1_Wheel()
{
	unsigned long microsData = micros();
	long Period_1;

	Period_1 =  microsData - oldMicros_1;
	oldMicros_1 = microsData;

	if (Period_1 == 0) Period_1 = 1;

	if (Period_1 > 1000000)
	{
		Period_1 = 1000000;
	}
	else
	{
		Period_1 *= -1*(M1_DIR);



		// Invert Signal if other side (XOR)
		if ((digitalRead(ENC1_INT) == HIGH) && (digitalRead(ENC1_IO) == HIGH))
		{
			Period_1 = -Period_1;
		}
		else if ((digitalRead(ENC1_INT) == LOW) && (digitalRead(ENC1_IO) == LOW))
		{
			Period_1 = -Period_1;
		}
	}

	Period_1_updated = true;

	Frequency1 = 1000000/Period_1;

	Distance1 += (perimeter*Frequency1);

	MotorSpeed1 = MotorSpeed1 + pid.UpdatePID1((DesiredFrequency1*2) - (int)Frequency1);

	if (MotorSpeed1 > 254) MotorSpeed1 = 254;
	if (MotorSpeed1 < -254) MotorSpeed1 = -254;

	if (automatic_motors_control) PWM_MOTOR_SET(MotorSpeed1, MotorSpeed2);

}


void Motoruino2SMotor::InterruptEncoder2_Wheel()
{
	unsigned long microsData = micros();
	long Period_2;

	Period_2 =  microsData - oldMicros_2;
	oldMicros_2 = microsData;


	if (Period_2 == 0) Period_2 = 1;

	if (Period_2 > 1000000)
	{
		Period_2 = 1000000;
	}
	else
	{
		Period_2 *= -1*(M2_DIR);



		// Invert Signal if other side (XOR)
		if ((digitalRead(ENC2_INT) == HIGH) && (digitalRead(ENC2_IO) == HIGH))
		{
			Period_2 = -Period_2;

		}
		else if ((digitalRead(ENC2_INT) == LOW) && (digitalRead(ENC2_IO) == LOW))
		{
			Period_2 = -Period_2;

		}
	}

	Period_2_updated = true;


	Frequency2 = 1000000/Period_2;

	Distance2 += (perimeter*Frequency2);

	MotorSpeed2 = MotorSpeed2 + pid.UpdatePID2((DesiredFrequency2*2) - (int)Frequency2);

	if (MotorSpeed2 > 254) MotorSpeed2 = 254;
	if (MotorSpeed2 < -254) MotorSpeed2 = -254;



	if (automatic_motors_control) PWM_MOTOR_SET(MotorSpeed1, MotorSpeed2);
}



void Motoruino2SMotor::InterruptTimer1_Wheel()
{

	if (!Timer1Dec--)
	{
		Timer1Dec = TIMER_RESET;

		if (Period_1_updated == false)
		{
			// Update with frequency = 0!!
			MotorSpeed1 =  MotorSpeed1 + pid.UpdatePID1(DesiredFrequency1)/2;

			if (MotorSpeed1 > 254) MotorSpeed1 = 254;
			if (MotorSpeed1 < -254) MotorSpeed1 = -254;

		} else Period_1_updated = false;


		if (Period_2_updated == false)
		{
			// Update with frequency = 0!!
			MotorSpeed2 = MotorSpeed2 + pid.UpdatePID2(DesiredFrequency2)/2;

			if (MotorSpeed2 > 254) MotorSpeed2 = 254;
			if (MotorSpeed2 < -254) MotorSpeed2 = -254;

		} else Period_2_updated = false;

		if (automatic_motors_control) PWM_MOTOR_SET(MotorSpeed1, MotorSpeed2);
	}

}

// ---------------------------------- PID Controller --------------------------------------------


Motoruino2SPID::Motoruino2SPID()
{
	Config(KP,KD,KI);
}


void Motoruino2SPID::Config(float _Kp, float _Kd, float _Ki)
{
	SetupPID1(_Kp, _Kd , _Ki);
	SetupPID2(_Kp, _Kd , _Ki);

	pidFirstTime1 = true;
	pidFirstTime2 = true;
}

void Motoruino2SPID::SetupPID1(float factorProportional, float factorDerivative, float factorIntegrative)
{
  pidFirstTime1 = true;
  pidSumCTE1 =0;
  factorP1 = factorProportional;
  factorD1 = factorDerivative;
  factorI1 = factorIntegrative;
  pidRetValue1 =0;
}

float Motoruino2SPID::UpdatePID1(float cte)
{

    if(pidFirstTime1)
    {
      pidFirstTime1 = false;
      pidPrevCTE1 = cte;
    }

    float proportional = cte * factorP1;

    float d = pidPrevCTE1 - cte;

    float derivative = d * factorD1;

    pidSumCTE1 += cte;
    float integrative = pidSumCTE1 * factorI1;

    pidRetValue1 = proportional + derivative + integrative;


  return pidRetValue1;
}

void Motoruino2SPID::SetupPID2(float factorProportional, float factorDerivative, float factorIntegrative)
{
  pidFirstTime2 = true;
  pidSumCTE2 =0;
  factorP2 = factorProportional;
  factorD2 = factorDerivative;
  factorI2 = factorIntegrative;
  pidRetValue2 =0;
}

float Motoruino2SPID::UpdatePID2(float cte)
{

    if(pidFirstTime2)
    {
      pidFirstTime2 = false;
      pidPrevCTE2 = cte;
    }

    float proportional = cte * factorP2;
    float d = pidPrevCTE2 - cte;
    float derivative = d * factorD2;

    pidSumCTE2 += cte;
    float integrative = pidSumCTE2 * factorI2;

    pidRetValue2 = proportional + derivative + integrative;


  return pidRetValue2;
}



