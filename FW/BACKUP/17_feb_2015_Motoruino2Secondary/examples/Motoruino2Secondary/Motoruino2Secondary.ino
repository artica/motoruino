#include "Arduino.h"
#include <avr/sleep.h>
#include <avr/interrupt.h>

#include "Metro.h"
#include "Wire.h"
#include "TimerOne.h"

#include "LSM9DS0.h"
#include "Motoruino2SMotor.h"
#include "Motoruino2SPower.h"
#include "Motoruino2SSPI.h"



// ----- DEFINES -----
#define GP_OUT A1

#define BLINKTIME 			1000
#define NAVIGATIONTIME 		10

#define ANGLE_OK 100			// Angle x 10 => example: 10deg = 1000 or 1deg = 100
#define ANGLE_MEDIUM 8000

#define BUFFER_SIZE 	10
#define WIRE_ADDRESS 	2

// ----- OBJECTS/GLOBAL -----
Motoruino2LSM9DS0				onboardIMU;
Motoruino2SMotor 	motor;		// Motor Functions
Motoruino2SPower 	power;		// Power Functions
Motoruino2SSPI		spi;		// SPI Functions

// Metro Update
Metro MetroUpdateTime = Metro(BLINKTIME);
Metro MetroNavigationTime = Metro(NAVIGATIONTIME);

// Wire Comm
char receivedBuffer[BUFFER_SIZE];
char sendBuffer[BUFFER_SIZE];

unsigned char sendLength = 0;

// Comm Protocol
enum _ReplyOrders { NO_REPLY , REPLY_CALIBRATE_IMU, REPLY_GET_HEADING, REPLY_GOTO_DEGREES};
enum _ReplyOrders ProcessLaterOrder = NO_REPLY;

// Pooling Update Flags
bool startFlagIMU = false; 				// Get IMU Values?
bool startNavigateToAngle = false;	// Goto Angle?
bool readyFlagToReply = false;			// Finished processing the reply?

// Navigation
int goalAngle = 0;
byte goalSpeed = 0;

// Aux Global Vars
long Data = 0;

// ----- SETUP -----
void setup() {


//   MCUCR |= (1<<BODS) | (1<<BODSE);
//   MCUCR &= ~(1<<BODSE);  // must be done right before sleep

	Serial.begin(115200);


	pinMode(GP_OUT, OUTPUT);
	pinMode(SLEEP_CTL, OUTPUT);
	digitalWrite(SLEEP_CTL, HIGH);


	// Comm Configuration
	commConfig();

	// Object Configurations

	motor.Config();
	motor.ConfigEncoders(motor.SLOW_ENCODER, 10, 4.25);
	power.Config();
	spi.config();
	onboardIMU.config();
	//onboardIMU.CalibrateGyro();

	// Pools/Flags init
	startFlagIMU = true;
	startNavigateToAngle = false;
	readyFlagToReply = false;;

	// Main Values init
	goalAngle = 0;
	goalSpeed = 0;

	// Aux Values init
	Data = 0;


	// set Motor 1 20
	//motor.SetSpeed(1,-15);
	//motor.SetSpeed(2,-15);

	motor.MOTOR_SET(0,0);
}


bool outstate = false;

// ----- LOOP -----
void loop() {

	outstate = !outstate;
	digitalWrite(GP_OUT, outstate);

	digitalWrite(SLEEP_CTL, HIGH);

//
//	sleep_enable();
//	/* 0, 1, or many lines of code here */
//	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
//	cli();
//	//sleep_bod_disable();
//	//sei();
//	sleep_cpu();
//	/* wake up here */
//	sleep_disable();


	// POOLING Functions
	// - IMU
	if (startFlagIMU)
	{
		onboardIMU.updateAccelerometer();
		onboardIMU.updateGyroscope();
		onboardIMU.updateMagnetometer();
	}

	// - Navigation
	if (startNavigateToAngle)					// Navigation Pooling function
		if (MetroNavigationTime.check() == 1)
			if (ReachAngle((long) goalAngle, goalSpeed)) startNavigateToAngle = false;

	// - Wire Communications (Master)
	UpdateComm();

	// TEST Functions to print
	if (MetroUpdateTime.check() == 1)
	{

//		Serial.print(onboardIMU.GetGyroHeading());		Serial.print('\t');
//		Serial.print(onboardIMU.GetAccelAvgZ());		Serial.print('\t');
//		Serial.print(onboardIMU.GetMagHeading());		Serial.print('\t');
//		Serial.print(onboardIMU.GetTemp());				Serial.print('\t');

//		Serial.print("D:");
//
//
		Serial.print(onboardIMU.getMagHeading());Serial.print("\t");
		Serial.println(onboardIMU.getGyroHeading());

//
//		Serial.print(motor.Distance1);Serial.print("\t");
//		Serial.print(motor.Distance2);Serial.print("\t");
//		Serial.print(motor.Frequency1);Serial.print("\t");
//		Serial.print(motor.Frequency2);Serial.print("\t");
//		Serial.println("");
//
//		motor.ResetDistance(1);
//		motor.ResetDistance(2);
	}

}





