/*
 * GyroComm.ino
 *
 *  Created on: Jan 16, 2015
 *      Author: brunoserras
 */

#include "Wire.h"



void commConfig()
{

	ProcessLaterOrder = NO_REPLY;

	Wire.begin(WIRE_ADDRESS);           // join i2c bus with address #4
	Wire.onReceive(InterruptReceiveI2C); 	// register Call Back Event/Function
	Wire.onRequest(InterruptRequestI2C); 	// register Call Back Event/Function
}

void UpdateComm()
{
	// REPLY TO MASTER (the received "Wire" Orders)
	switch (ProcessLaterOrder)
	{
		case NO_REPLY:
			break;

		case REPLY_CALIBRATE_IMU:

			if (onboardIMU.CalibrateGyro()){			// Test if it has ended
				sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
				Serial.print("CALIB OK");
			}
			else
			{
				sendBuffer[4] = 'N'; sendBuffer[5] = 'O';
				Serial.print("CALIB NOK");
			}

			readyFlagToReply = true;
			ProcessLaterOrder = NO_REPLY;
			break;

		case REPLY_GET_HEADING:
			Data = onboardIMU.GetGyroHeading();				// Reads the Value

			//Serial.println(Data);

			for (int x = 0; x < 4; x++ )				// Fills the Buffer MSB goes first
			{
				sendBuffer[4 + x] = (0x000000FF & Data);
				Data = Data >> 8;
			}
			sendLength = 4 + 4;							// Buffer Size to Reply 4 (cmd) + 4 (data)

			readyFlagToReply = true;
			ProcessLaterOrder = NO_REPLY;
			break;

		case REPLY_GOTO_DEGREES:
			// Get the Angle From the Buffer
			Data = 0;
			for (int i = 4; i < 6; i++)
			{
				Data = Data | (receivedBuffer[i] << ((i-4)*8));
			}

			goalSpeed = receivedBuffer[6];
			goalAngle = Data;

			// Get the Speed and Runs ReachAngle Navigation Function
			if (ReachAngle((long) goalAngle, goalSpeed))
			{
				// Sets the ReplyBuffer
				sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
				startNavigateToAngle = false;
			} else
			{
				// Sets the ReplyBuffer
				sendBuffer[4] = 'N'; sendBuffer[5] = 'O';
				startNavigateToAngle = true;
			}

			sendLength = 4 + 2;
			readyFlagToReply = true;
			ProcessLaterOrder = NO_REPLY;
			break;

		default:
			break;
	};
}




// Received Command Parser - ISR Routine
void CommParseCommand(unsigned short howMany)
{
	readyFlagToReply = false;

	// Set PWM "SPWM"

	if (are_equal((char*)"SPWM", receivedBuffer, 0, 4))
	{
                startNavigateToAngle = false;
		motor.PWM_Value_M1_IN1 = receivedBuffer[4];
		motor.PWM_Value_M1_IN2 = receivedBuffer[5];
		motor.PWM_Value_M2_IN1 = receivedBuffer[6];
		motor.PWM_Value_M2_IN2 = receivedBuffer[7];

		analogWrite(M1_IN1, motor.PWM_Value_M1_IN1);
		analogWrite(M1_IN2, motor.PWM_Value_M1_IN2);
		analogWrite(M2_IN1, motor.PWM_Value_M2_IN1);
		analogWrite(M2_IN2, motor.PWM_Value_M2_IN2);
	} else

	// Calibrate Gyro "CALG"
	if (are_equal((char*)"CALG", receivedBuffer, 0, 4))
	{
		Serial.println("CalibrateIMU");
		digitalWrite(M1_IN1, LOW);
		digitalWrite(M1_IN2, LOW);
		digitalWrite(M2_IN1, LOW);
		digitalWrite(M2_IN2, LOW);

		// Copies the Command for the Reply
		for (int x = 0; x < 4; x++) sendBuffer[x] = receivedBuffer[x];
		ProcessLaterOrder = REPLY_CALIBRATE_IMU;						// Sets the order for reply
	} else

	// START GYRO "STTG"
	if (are_equal((char*)"STTG", receivedBuffer, 0, 4))
	{
		Serial.println("StartIMU");

		for (int x = 0; x < 4; x++)
			sendBuffer[x] = receivedBuffer[x];

		sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
		sendLength = 4 + 2;							// Buffer Size to Reply 4 (cmd) + 2 (data)

		startFlagIMU = true;
		readyFlagToReply = true;

	} else

	// STOP GYRO "STPG"
	if (are_equal((char*)"STPG", receivedBuffer, 0, 4))
	{
		Serial.println("StopIMU");

		for (int x = 0; x < 4; x++)
			sendBuffer[x] = receivedBuffer[x];

		sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
		sendLength = 4 + 2;							// Buffer Size to Reply 4 (cmd) + 2 (data)

		startFlagIMU = false;
		readyFlagToReply = true;

	} else

	// GET HEADING "GHED"
	if (are_equal((char*)"GGHD", receivedBuffer, 0, 4))
	{

		// Copies the Command for the Reply
		for (int x = 0; x < 4; x++) sendBuffer[x] = receivedBuffer[x];
		ProcessLaterOrder = REPLY_GET_HEADING;						// Sets the order for reply

	} else
	// RESET HEADING "RSTH"
	if (are_equal((char*)"RSTH", receivedBuffer, 0, 4))
	{
			Serial.println("ResetHeading");
			onboardIMU.ResetGyro(true, false, false);

			for (int x = 0; x < 4; x++)
				sendBuffer[x] = receivedBuffer[x];

			sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
			sendLength = 4 + 2;							// Buffer Size to Reply 4 (cmd) + 2 (data)

			readyFlagToReply = true;

	} else
	// RESET GYRO "RSTG"
	if (are_equal((char*)"RSTG", receivedBuffer, 0, 4))
	{
			Serial.println("ResetGyro");
			onboardIMU.ResetGyro(true, true, true);

			for (int x = 0; x < 4; x++)
				sendBuffer[x] = receivedBuffer[x];

			sendBuffer[4] = 'O'; sendBuffer[5] = 'K';
			sendLength = 4 + 2;							// Buffer Size to Reply 4 (cmd) + 2 (data)

			readyFlagToReply = true;
	} else
	// GOTO DEGREES "GDEG"
	if (are_equal((char*)"GDEG", receivedBuffer, 0, 4))
	{
		// Copies the Command for the Reply
		for (int x = 0; x < 4; x++) sendBuffer[x] = receivedBuffer[x];
		ProcessLaterOrder = REPLY_GOTO_DEGREES;						// Sets the order for reply
	}


}


// Callback - Received I2C from Master Atmel32u4
static void InterruptReceiveI2C(int howMany)
{
  unsigned char ReceivedDataPointer = 0;

  // if received more than it can handle returns
  if (howMany > BUFFER_SIZE) return;

  // Flush the Buffer from wire
  while(0 < Wire.available()) receivedBuffer[ReceivedDataPointer++] = Wire.read();

  // Parse to launch launch the order
  CommParseCommand(howMany);
}


// Callback - Will Send I2C to Master Atmel32u4
static void  InterruptRequestI2C()
{
	if (readyFlagToReply)
	{
		Wire.write(sendBuffer, sendLength);
	}
	else
	{
		Serial.println("NO_DATA");
		Wire.write("NO DATA");
	}

}

bool are_equal(char *s1, char *s2, unsigned char start, unsigned char size)
{

    for (int i = size; i--; )
    {
        if (s1[i+start] != s2[i+start]) return false;  // They are different
    }
    return true;  // They must be the same
}


