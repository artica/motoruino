
final static int LINE_THICKNESS = 10;
import processing.serial.*;

Serial myPort;  // Create object from Serial class
String val;      // Data received from the serial port

void setup() 
{
  size(400, 400);
  String portName = Serial.list()[3];
  myPort = new Serial(this, portName, 115200);
}

float radians;

void drawAngle(String value)
{
    background(51);
    
    if (value != null) value = value.trim();
    int int_value
    = 0;
    float angle = 0;
    
    try
    {
        int_value =  Integer.parseInt(value);
      //  angle = int_value/570;    // 500 dps
      //  angle = -int_value/140;    // 2000 dps      
       //  angle = int_value /100;    // 2000 dps
      //  angle = int_value/1140;    // 250 dps
      
       angle = int_value; //* 360 / 32768;    // 2000 dps
        radians = 2*PI*angle/360;
    }
    catch (Exception e) 
    {
        int_value = 0;
        println("fail:"+value);
    }

    println(value+"  "+int_value+"  "+angle);
    
    
    translate(width/2, height/2);
    rotate(radians+PI);
    translate(-LINE_THICKNESS/2, 0);
    rect(0, 0, LINE_THICKNESS, 200);   
    
}

void draw()
{
    if ( myPort.available() > 0) 
    {  // If data is available,
    val = myPort.readStringUntil('\n');         // read it and store it in val
    }
    
    drawAngle(val);

}

